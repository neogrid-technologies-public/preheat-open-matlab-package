% Runs a demo of the API capabilities
clear all;
clc;
close all;

%%  Just a quick check that your Matlab version is sufficiently recent
if verLessThan('matlab', '9.9') == 1
    warning('Support for Matlab versions prior to r2020b is not guaranteed. Please update to latest if you can.')
end
    
%% First, you need to add the toolbox to the Matlab path.
%
% (This needs to be done in every script where you use the toolbox, 
%  unless you make a permanent change to Matlab's path)

% Add the Matlab toolbox to the Matlab path
path_to_Matlab_toolbox = 'C:\[path to your toolbox]\PreHEAT_API\Matlab';
addpath(genpath(path_to_Matlab_toolbox));


%% Then you need to load your API key.
%
% (This needs to be done at the beginning of every script where you use the toolbox)
%
%   If you don't have an API key, you can create your own on:
%       https://app.neogrid.dk/icebear/#!/app/user/profile 
%
%   If you forget this line in your script, then Matlab will open a pop up
%       window asking you for your API key the first time it is needed.
%

% The key provided here is a demo/test key
your_API_key = 'KVkIdWLKac5XFLCs2loKb7GUitkTL4uJXoSyUFIZkVgWuCk8Uj';

PreHEAT_API.ApiConfig.setApiKey(your_API_key);


%% Overview of the buildings that you have access to via the API.
%
% In the toolbox typology, a group of buildings is called a "Pool", which
%   is created with the following command:
my_pool = PreHEAT_API.Pool();

% This method allows you to visualise the details of your pool
my_pool.getDetails()

% This method allows you to get an overview of the units that you have
% access to: 
%
% (restricting to maximum 10 latest locations, due to speed - this can be omitted) 
location_ids_focus = my_pool.locationIds(1);

my_pool.getUnitsOverview(location_ids_focus)



%% Select a location id for the tests at building level

% In the toolbox framework, buildings are identified by a so-called
% 'locationId' which is a unique identifier for a building in Neogrid's
% system.
%
%   This is typically a 4 numbers digit (e.g. 2080)
%   (By default, the code here uses the first available building)
locationId = my_pool.locationIds(1);


% A building structure is modelled as a 'Building' object
my_building = PreHEAT_API.Building(locationId);

% The building object contains details of the geographical location
my_building.location

% The building contains units modelling the different elements of the HVAC
% installation
my_building.units

% And it also contains details of the weather data (latest available local
% forecast from www.yr.no, rather than historical measurements)
my_building.weather


% It also contains details about the heater area (m2) 
my_building.area
%   and the number of apartments
my_building.apartments

% If zones have been defined, then they are also contained in the building
% object
my_building.zones

% And you can get structured zone information for later use by using:
zones_details = my_building.getZonesDetails(); % Get the details of the different zones


%% Loading data from the building and its units

% Loading data requires that you first define a start and end date in
% datetime format.
%
%   Obs: Remember to define the timezone (by default, it will be set to UTC)
start_date = datetime([2021 5 1 0 0 0],'TimeZone','Europe/Copenhagen'); 
end_date   = datetime([2021 5 7 0 0 0],'TimeZone','Europe/Copenhagen'); 

% You also need to specify a time resolution
%   valid options are:
%       'minute', 'hour', 'day', 'week', 'month', 'year'
%
%   Obs: 'minute' corresponds to 5 minutes data and is only available for
%       building measurements (not weather data which is only available with hourly resolution and above)
time_resolution = 'hour';         

% You can then specify specific options for the loading of the data in the
% building object.
options2use = struct();

% You can either choose to load all data at the unit level (1 - default) or at
% subUnit level (0)
options2use.loadSubcomponentsInUnit = 0;
options2use.loadSubcomponentsInUnit = 1;

% You can also select only specific components to load (very recommended to
% speed up the execution, rather than loading every possible measurement on
% the building ('*' - default) )
options2use.limitComponents = '*';
options2use.limitComponents = {'main.supplyT','heating.secondaries.returnT','main.energy','weather.Temperature' };

% To get a list of available measurements to select from, execute:
my_building.getAllComponentIds()    % For box component data 
my_building.getAllWeatherTypeIds()  % For weather data 

%% Loading data for the whole building

% You can load data for the whole building by calling:
my_building.loadData( start_date, end_date, time_resolution, options2use);

% Then you can access data loaded for the the different units by calling
% the 'getData' method.

% (for example here for the main meter)
T_unit      = my_building.units.main.getData()

% (for example here for the weather)
T_weather   = my_building.weather.getData()

% And then you can access the data in timetable format using the 'data'
% parameter
T_unit.data
T_weather.data

% For any Data object, you can get information about the signal units by calling
T_weather.getDataUnits()
T_unit.getDataUnits()

% And you can clear all the data loaded for the building by calling:
my_building.clearData();

%% Loading the data only for a specific unit

% If you only need data for a specific unit, you can just call the method
% on the relevant unit (e.g. the main meter)

%   For a unit of the building
my_building.units.main(1).loadData( start_date, end_date, time_resolution );

%   For the weather data
my_building.weather.loadData( start_date, end_date, time_resolution);


%% Sending a schedule to a control unit

% Select the control unit that you want to focus on
control_unit_test = my_building.units.heating.subUnits(1).controlUnits;

% Check is if is active (synchronise with the gateway and ready to control)
control_unit_test.isActive()

%------------- Creating a schedule --------------------------------------
% Then you need to create a schedule, structured in a dataframe format. A schedule consists in a dataframe containing:
% - *startTime*: this is the sequence of times when the gateway will need 
%       to apply a new value to the system
% - *value*: this is the sequence of the value that will be applied by the gateway 
%       at the corresponding step in "startTime" (note that the last value is a fallback value)
% - *operation*: this is the operation mode, in most cases, simply omit this field
%       leave it set to "NORMAL" for each step (an omitted value for this field 
%       will default to "NORMAL").
% 
% **Important notes**:
% - As the control is happening over the internet, it is important to account 
%       for a risk of temporary loss of connection between your control and the gateway. 
%       Therefore, it is better to send a schedule of the several hours ahead.
% - Whenever a new schedule is received, the system forgets all previously 
%       received scheduled values starting from the first timestep of the new schedule.
% - The last value of the schedule is treated as a fallback, which will be 
%       applied 'ad vitam aeternam' by the gateway until a new value is received. 
%       It is therefore important that it is a safe and satisfactory default value.


% Creating a dummy schedule (it must have a "startTime" column filled with
% datetimes, including timezone, and a "value" column containing numbers) 
N = 3;
startTime= datetime('now','TimeZone','Europe/Copenhagen')+hours([1:N]');
value    = 10+[1:N]';
schedule2write = timetable( startTime, value )

% Sending the schedule to the control unit
% **WARNING**: this line overwrites existing control values, only use is 
%   after agreement with the building owner and having checked that 
%   your schedule is acceptable)
control_unit_test.setSchedule(schedule2write)

% Checking that the writing has worked
actual_schedule_on_component = ...
    control_unit_test.getSchedule(startTime(1)-hours(1), startTime(end)+hours(1))
