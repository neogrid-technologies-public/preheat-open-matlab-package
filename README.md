# PreHEAT open Matlab package

Open Matlab package to consume Neogrid REST API.

## Table of contents
* [Installation of the toolbox](#installing-the-toolbox-matlab)
* [Testing the installation](#testing-the-toolbox-matlab)
* [Functionalities](#using-the-toolbox-matlab)
* [Troubleshooting](#troubleshooting-matlab)
* [Web API details](#web-api-details)

## Installing the toolbox (Matlab)
In order to install the toolbox, you need to first do the following:
1. Get a copy of your API key on your [user profile](https://app.neogrid.dk/icebear/#!/app/user/profile)
2. Identify a folder to which the toolbox should be installed (it is recommended to only have it in one place rather than duplicating it between projects) - and make sure that the folder is [added permanently to the Matlab path](https://se.mathworks.com/matlabcentral/answers/116177-how-to-add-a-folder-permanently-to-matlab-path).
3. Copy/paste the folder "+PreHEAT" to the folder identified in step (2).


## Using the toolbox (Matlab)
To get an overview of the commands available and the possibilities, you can have a look at the following script and its comments :

```
PreHEAT_API_demo.m
```

## Testing the toolbox (Matlab)
This section is not required for daily use of the toolbox, but more for advanced users who would like to update the code.

To test the toolbox, run the following command in Matlab:
```
PreHEAT_API.run_test
```
If the test fails, make sure that it is using a location ID that you have access to.

## Troubleshooting (Matlab)
If you encounter any issues, please get in touch by email to pvf@neogrid.dk (to facilitate debugging, please attach a screenshot of your error message).

## Web API details
For the documentation of the raw Web API access, please refer to our [open documentation page](https://neogrid-technologies.gitlab.io/neogrid-api/).
