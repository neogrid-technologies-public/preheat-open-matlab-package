%
%   WARNING: This class is deprecated. This class will be removed in the
%       future and functionalities ported to a new class.
%
classdef ComfortProfile < handle
    %ComfortProfile Defines a comfort profile in the PreHEAT sense
    
    properties (GetAccess='public', SetAccess='protected')
        id
        name
    end
    
    properties (GetAccess='protected', SetAccess='protected')
        lastUpdated
        schedules
        scheduleMode
    end
    
    methods (Access='public')
        
        function obj = ComfortProfile(comfort_profile_structure)            
            % Constructor creating a ComfortProfile object corresponding to a
            % comfort profile as returned from an API call.
            %
            % Inputs:
            %       comfort_profile_structure     Structure containing the
            %           details of the comfort profile (as returned from the API)
            %        
            
            error('This functionality is deprecated.');
            
            fields2use = fieldnames(comfort_profile_structure);
            
            for j=1:length(fields2use)
                
                field_j = fields2use{j};
                                
                switch field_j                    
                    case 'id'
                        obj.id	= comfort_profile_structure.id;
                        
                    case 'name'
                        obj.name= comfort_profile_structure.name;
                        
                    case 'lastUpdated'
                        obj.lastUpdated = PreHEAT_API.ApiConfig.convertFromApiOutputDate(comfort_profile_structure.lastUpdated);
                        
                    case 'schedules'
                        obj.schedules = comfort_profile_structure.schedules;
                        
                    case 'scheduleMode'
                        obj.scheduleMode    = comfort_profile_structure.scheduleMode;
                        
                    otherwise                        
                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                            sprintf('Loading of field "%s" is not yet implemented.', field_jj),...
                            'ComfortProfile' );                        
                end
            end
        end
        
        function id = getId(obj)
            id = obj.id;
        end
        function last_update = getLastUpdate(obj)
            last_update = obj.lastUpdated;
        end
        
        function schedule = getSchedule(obj)
            error('This function is not implemented yet');
            schedule = [];
        end
        
        function profile_is_active = isActive(obj)
            error('This function is not implemented yet');
            profile_is_active = NaN;
        end
        
    end
end
