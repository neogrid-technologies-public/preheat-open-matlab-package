classdef ApiConfig
    %API_config Class taking care of exchange with the API
    %
    
    % These settings can be adjusted by the toolbox user if needed
    properties (Constant,GetAccess='public')   
        apiMaxCidsPerCall   = 95; % Maximum command ids per call    (recommended value: 95)
        apiMaxPointsPerCall = 85000; % Maximum command ids per call (recommended value: 85000)
    end
    
    properties (Constant,GetAccess='private')   
        apiTimeOut          = 60; % Number of seconds before timeout (double) (recommended value: 60)
        retryIfFails        = 3;  % Number of allowed trials if an API call fails
        maxReleaseTolerance = 180;% Maximum allowed duration of a release on the API server (seconds) (recommended value: 180)
        
        % These settings are defined according to the API specification (must not be changed)          
        apiTimezone         = 'UTC';                                    % Timezone of times delivered by the API (string)
        apiDateOutputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";             % String format for the times delivered by the API (string)
        apiDateInputFormat  = 'yyyy-mm-ddTHH:MM:SSZ';                   % String format for the times provided to the API as arguments in calls - ISO 8601 format (string)
        apiCharEncoding     = 'UTF-8';                                  % Character encoding in the API (string)
        
        % These are the names of the environment variables used in this class
        envApiKey                   = 'NEOGRID:PREHEAT_API:API_KEY';
        envRunningOnNeogridsNetwork = 'NEOGRID:PREHEAT_API:RUNNING_ON_NEOGRID_INTERNAL';
        envOnlyFastOperationAllowed = 'NEOGRID:PREHEAT_API:AUTOMATIC_CONTROL_MODE';
    end
                
    % The class is not meant to be instantiated
    methods (Access='public')
    end    
    methods (Access='private')
        function obj = ApiConfig(varargin)
        end
    end
    
    methods (Static, Access='public')
        
        function setApiKey(API_key2use)
            %setApiKey  Setting the API key for this instance of execution.
            %
            % Valid calls
            %   setApiKey()
            %   setApiKey(API_key2use)
            %
            % Inputs:
            %       API_key2use  API key to use (char) - if not provided,
            %                       prompts the user to enter the key in a
            %                       dialog window
            %
            
            % Creating the API key if it doesn't exist already
            if nargin<1
                % Getting the key from user input
                API_key2use     = inputdlg('Please insert your API key','API key');
                API_key2use   = API_key2use{1};                
            end
            
            % Checking inputs
            if ~ischar(API_key2use)
                error('The API key must be a string');
            end
            
            % Writing the new API key in the environment variables
            setenv(PreHEAT_API.ApiConfig.envApiKey,API_key2use);            
        end
        
        function setOperationMode( mode2use )
            %setOperationMode   Method to set the operation mode of the
            %toolbox.
            %
            % Valid calls:
            %   setOperationMode('normal')
            %   setOperationMode('automatic')
            %
            % Details:
            %   normal      Normal operation of the toolbox
            %   automatic   Disables checks and failsafes to allow fastest
            %                   possible operation.
            
            switch mode2use
                case 'automatic'
                    env_var2set = 'AUTOMATIC';
                    
                case 'normal'
                    env_var2set = 'NORMAL';
                    
                otherwise 
                    error('Illegal operation mode (%s)',mode2use);
            end
            
            % Writing the new API key in the environment variables
            setenv(PreHEAT_API.ApiConfig.envOnlyFastOperationAllowed,env_var2set);  
        end
        
        function call_result = apiGet(apiRequest, return_format)
            %apiGet     Method to carry out an API call with a given GET request.
            %
            % Valid calls
            %   call_result = apiGet(apiRequest)
            %   call_result = apiGet(apiRequest,'json')
            %   call_result = apiGet(apiRequest,'csv')
            %
            % Inputs:
            %       apiRequest      Request to the API (string, excluding base
            %                           URL and API key which are added automatically in the call) 
            %                           i.e. the actual request sent will look like
            %                           https://api.neogrid.dk/public/api/vX/[apiRequest]api_key=X
            %
            %       return_format   Format of the response from the API (default: 'json')
            %
            % Outputs:
            %       call_result     Result of the call (struct format)
            %
            
            if nargin<2
                return_format = 'json';
            end
                        
            % Matlab specific attribute for calls
            apiOptions          = weboptions(...
                'KeyName',  'Authorization',...
                'KeyValue', ['Apikey ',PreHEAT_API.ApiConfig.getApiKey()],...
                'TimeOut',              PreHEAT_API.ApiConfig.apiTimeOut, ...
                'CharacterEncoding',    PreHEAT_API.ApiConfig.apiCharEncoding );
            
            switch return_format
                case 'csv'
                    apiOptions.MediaType    = 'application/json';
                    apiOptions.ContentType  = 'table';
                    apiOptions.HeaderFields = {'Accept', 'text/csv'};
                    apiOptions.ContentReader = @readtable;
                    
                case 'json'
                    apiOptions.MediaType    = 'application/json';
                    apiOptions.ContentType  = 'json';
                    apiOptions.HeaderFields ={'Content-Type' 'application/json'};
                    apiOptions.ContentReader = @(x) jsondecode(fileread(x));
                    
                otherwise
                    error('Unsupported value of "return_format" (%s)',return_format);
            end
            call_string2use = sprintf('%s/%s',PreHEAT_API.ApiConfig.getApiUrl(),...
                apiRequest);
            
            % Short-circuiting fault ride through for fast execution
            if PreHEAT_API.ApiConfig.onlyFastOperationAllowed()
                % Not allowing pause or exception handling in automatic operation 
                % (to avoid disruption)
                call_result = webread( call_string2use,apiOptions );
                
            else
                % Else enabling the repetitions and fault ride-through
                n_trials = 0;
                n_trials_allowed = PreHEAT_API.ApiConfig.retryIfFails;
                went_OK  = 0;
                error_diagnostic = [];
                while n_trials<n_trials_allowed && ~went_OK
                    
                    n_trials = n_trials + 1;
                    
                    try
                        t_call_made_debug = datetime('now','TimeZone','UTC');
                        
                        % Enable resilience to releases
                        PreHEAT_API.ApiConfig.releaseRideThrough(n_trials,error_diagnostic);
                        
                        % Sends the call to the API
                        try
                            call_result = webread( call_string2use,apiOptions );
                        catch e_call_API
                            switch e_call_API.identifier
                                case 'MATLAB:webservices:ContentTypeMismatch'
                                    % In the case of an error to the API, to handle
                                    % the fact that Matlab is not able to hande
                                    % an HTML return with the settings, remake
                                    % the call in a more flexible way.
                                    apiOptions_recall = apiOptions;
                                    apiOptions_recall.ContentType  = 'auto';
                                    apiOptions.ContentReader = [];
                                    call_result = webread( call_string2use, apiOptions_recall );
                                    warning('Content of data returned by the server did not match expectations - retrying in "auto" mode.');
                                otherwise
                                    rethrow(e_call_API);
                            end
                        end
                        went_OK = 1;
                        error_diagnostic = [];
                        
                    catch e
                        error_diagnostic = e;
                        if ~PreHEAT_API.ApiConfig.exceptionWasTimeout(error_diagnostic)
                            break;
                        end
                    end
                end
                if ~isempty(error_diagnostic)
                    t_timeout           = datetime('now','TimeZone','UTC');
                    debug_time_format   = 'yyyy-mm-dd HH:MM:SS:FFF';
                    e_extra_info = MException('ApiCall:Failed',sprintf('API call made at UTC:"%s" failed at UTC:"%s" (trial %i)', ...
                        datestr(t_call_made_debug,debug_time_format), datestr(t_timeout,debug_time_format), n_trials ));
                    error_diagnostic = error_diagnostic.addCause(e_extra_info);
                    PreHEAT_API.ApiConfig.raiseError(error_diagnostic, call_string2use);
                end
            end
        end
        
        function call_result = apiPut(apiRequest, data2write)
            %apiPut     Method to carry out an API call with a given PUT request.
            %
            % Valid calls
            %   call_result = apiPut(apiRequest, data2write)
            %
            % Inputs:
            %       apiRequest      Request to the API (string, excluding base
            %                           URL and API key which are added automatically in the call) 
            %                           i.e. the actual request sent will look like
            %                           https://api.neogrid.dk/public/api/vX/[apiRequest]api_key=X
            %
            %       data2write      Data to send to the API (char or struct - converted to JSON)
            %
            % Outputs:
            %       call_result     Result of the call (struct format)
            %
                        
            if isstruct(data2write)
                data2write = jsonencode( data2write );
            elseif ischar(data2write)
                % Nothing to do
            else
                error('Illegal "data2write" input parameter (must be either a JSON string or a struct).');
            end
                        
            % Matlab specific attribute for calls
            apiOptions          = weboptions(...
                'KeyName',  'Authorization',...
                'KeyValue', ['Apikey ',PreHEAT_API.ApiConfig.getApiKey()],...
                'MediaType',            'application/json',...
                'ContentType',          'json',...
                'TimeOut',              PreHEAT_API.ApiConfig.apiTimeOut, ...
                'CharacterEncoding',    PreHEAT_API.ApiConfig.apiCharEncoding, ...
                'RequestMethod',        'put' );
            call_string2use = sprintf('%s/%s',PreHEAT_API.ApiConfig.getApiUrl(),...
                apiRequest );
            
            % Short-circuiting fault ride through for fast execution
            if PreHEAT_API.ApiConfig.onlyFastOperationAllowed()
                % Not allowing pause or exception handling in automatic operation
                % (to avoid disruption)
                call_result = webwrite( call_string2use,data2write,apiOptions );
                
            else
                % Else allowing the exception management and fault ride-through
                n_trials = 0;
                n_trials_allowed = PreHEAT_API.ApiConfig.retryIfFails;
                went_OK  = 0;
                error_diagnostic = [];
                while n_trials<n_trials_allowed && ~went_OK
                    
                    n_trials = n_trials + 1;
                    
                    try
                        t_call_made_debug = datetime('now','TimeZone','UTC');
                        
                        % Enable resilience to releases
                        PreHEAT_API.ApiConfig.releaseRideThrough(n_trials,error_diagnostic);
                        
                        % Sends the call to the API
                        call_result = webwrite( call_string2use,data2write,apiOptions );
                        went_OK = 1;
                        error_diagnostic = [];
                        
                    catch e
                        error_diagnostic = e;
                        if ~PreHEAT_API.ApiConfig.exceptionWasTimeout(error_diagnostic)
                            break;
                        end
                    end
                end
                if ~isempty(error_diagnostic)
                    t_timeout           = datetime('now','TimeZone','UTC');
                    debug_time_format   = 'yyyy-mm-dd HH:MM:SS:FFF';
                    e_extra_info = MException('ApiCall:Failed',sprintf('API call made at UTC:"%s" failed at UTC:"%s" (trial %i)', ...
                        datestr(t_call_made_debug,debug_time_format), datestr(t_timeout,debug_time_format), n_trials ));
                    error_diagnostic = error_diagnostic.addCause(e_extra_info);
                    PreHEAT_API.ApiConfig.raiseError(error_diagnostic, call_string2use);
                end
            end
        end
        
        function date2use = convertFromApiOutputDate(date2use) 
            %convertFromApiOutputDate   Method to carry out an conversion from 
            %   API times to Matlab datetime.
            %
            % Valid call:
            %   date2use = convertFromApiOutputDate(date2use)
            %
            % Inputs:
            %       date2use   Dates returned by the API (string array)
            %
            % Outputs:
            %       date2use   Corresponding times with timezone (Matlab datetime)
            %
            
            if isnumeric(date2use)
                % Do nothing
            else
                date2use = datetime(date2use,...
                    'InputFormat',  PreHEAT_API.ApiConfig.apiDateOutputFormat,...
                    'TimeZone',     PreHEAT_API.ApiConfig.apiTimezone );
            end
        end
        
        function converted_date = convertToApiInputDate(date_to_api)
            %convertToApiInputDate  Method to carry out an conversion from 
            %   API times to Matlab datetime.
            %
            % Valid call:
            %   converted_date = convertToApiInputDate(date_to_api)
            %
            % Inputs:
            %       date_to_api     Matlab times to use in the API call (Matlab datetime array)
            %
            % Outputs:
            %       converted_date  Corresponding times in appropriate format for use in API calls (string array)
            %
            
            date_to_api.TimeZone= PreHEAT_API.ApiConfig.apiTimezone;
            converted_date      = datestr( date_to_api,  PreHEAT_API.ApiConfig.apiDateInputFormat );
        end
                
        function [only_fast_operation_allowed] = onlyFastOperationAllowed()
            %onlyFastOperationAllowed   Method to evaluate whether the
            % operation should be made fastest possible, 
            % in which case most failsafe will therefore discarded. 
            %
            % Valid calls:
            %   [only_fast_operation_allowed] = onlyFastOperationAllowed()
            %
            % Outputs:
            %   only_fast_operation_allowed   Boolean (true=disable checks, 
            %       false=enable all checks)
            %
            
            % Writing the new API key in the environment variables
            operation_mode = getenv( PreHEAT_API.ApiConfig.envOnlyFastOperationAllowed );  
            
            if ~isempty(operation_mode) && strcmp(operation_mode,'AUTOMATIC')
                only_fast_operation_allowed = true;
                
            elseif isempty(operation_mode) || strcmp(operation_mode,'NORMAL')
                only_fast_operation_allowed = false;
                
            else
                error('Illegal value of the environment variable "%s" (%s)',...
                    PreHEAT_API.ApiConfig.envOnlyFastOperationAllowed, operation_mode);
            end
        end
        
        function [string_vector] = numericVectorToString( num_vector, separator )
            %numericVectorToString Method to carry out an conversion from API times to Matlab datetime.
            %
            % Valid calls:
            %   [string_vector] = numericVectorToString(num_vector)
            %   [string_vector] = numericVectorToString(num_vector,separator)
            %
            % Inputs:
            %       num_vector      Numeric vector to convert to string
            %       separator       Separation character to use in string (default: ',')
            %
            % Outputs:
            %       string_vector   Char corresponding to the collated vector 
            %                           with separator between elements
            %
            
            if nargin<2
                separator = ',';
            end
            
            if isempty(num_vector)
                string_vector = '';
                return;
            elseif ~isnumeric(num_vector)
               error('Input must be a numeric vector/column'); 
            elseif iscolumn(num_vector)
                num_vector = num_vector';
            elseif isrow(num_vector)
                % Nothing to do
            else
               error('Matrices are not supported'); 
            end
            
            string_vector = regexprep(num2str(num_vector),'[^0-9.]+',separator);
        end
        
        function flagUpdateRequired(flag_details,class_name)
            
            if PreHEAT_API.ApiConfig.onlyFastOperationAllowed()
                return;
            end
            
            warning('[PreHEAT_API.%s] %s',class_name,flag_details);
        end
        
        function [cleaned_string] = removeSensitiveContentFromString(input_string)
            %removeSensitiveContentFromString   Method to remove sensitive
            %   content (e.g. API key, etc) from a string.
            %
            % Valid calls:
            %   [cleaned_string] = removeSensitiveContentFromString(input_string)
            %
            % Inputs:
            %       input_string    Char string to remove sensitive content from
            %
            % Outputs:
            %       cleaned_string  Char string with removed sensitive content
            %
            
            % Remove API key from the string
            cleaned_string = strrep(...
                convertStringsToChars(input_string),...
                PreHEAT_API.ApiConfig.getApiKey(),...
                '[hidden_API_key]');            
        end
    end
    
    methods (Static, Access='private')
        
        function [api_key] = getApiKey()
            % getApiKey     Method to load the API key from the environment variables 
            api_key = '';
            
            while isempty(api_key)                
                api_key = getenv(PreHEAT_API.ApiConfig.envApiKey);
                
                if isempty(api_key)
                    PreHEAT_API.ApiConfig.setApiKey();
                end
            end
        end
        
        function api_base_url = getApiUrl()
            
            if ~PreHEAT_API.ApiConfig.isRunningOnNeogridsInternalNetwork()
                % Outside of Neogrid's premises, using public hostname
                api_base_url = 'https://api.neogrid.dk/public/api/v1';                
            else
                % For run within Neogrid's network, using local hostname
                api_base_url = 'https://chewbacca.neogrid.dk/public/api/v1';  
            end
        end
                
        function raiseError(error2use, api_call_string)
            
            call_string2use = PreHEAT_API.ApiConfig.removeSensitiveContentFromString(api_call_string);
            
            switch error2use.identifier
                
                case 'MATLAB:webservices:HTTP400StatusCodeError'
                    msgID   = 'PreHEAT:API:invalidRequest';
                    msgtext = sprintf('PreHEAT_API: The API call is invalid (%s).', ...
                    call_string2use );
                
                case 'MATLAB:webservices:HTTP403StatusCodeError'
                    msgID   = 'PreHEAT:API:invalidKey';
                    msgtext = sprintf('PreHEAT_API: The API key (%s) is invalid.', PreHEAT_API.ApiConfig.getApiKey() );
                    
                case 'MATLAB:webservices:HTTP404StatusCodeError'
                    msgID    = 'PreHEAT:API:invalidRequest';
                    msgtext = sprintf('PreHEAT_API: The API url call (%s) is invalid.', ...
                        call_string2use);
                    
                case 'MATLAB:webservices:HTTP503StatusCodeError'
                    msgID    = 'PreHEAT:API:unavailable';
                    msgtext = sprintf('PreHEAT_API: The API call resulted in a service unavailable. Check https://status.neogrid.dk/ to verify status of services and check the call (%s)',...
                        call_string2use);
                    
                case 'MATLAB:webservices:Timeout'
                    msgID    = 'PreHEAT:API:timeout';
                    msgtext = sprintf('PreHEAT_API: The API call resulted in a TimeOut. Increase "ApiConfig.apiTimeOut" if needed, or check the call (%s)',...
                        call_string2use);
                    
                otherwise
                    error( '[%s] %s', error2use.identifier, error2use.message );
            end
            
            for i=1:length(error2use.cause)
                cause_i = error2use.cause{i};
                
                msgtext = sprintf('%s \n(extra cause: [%s] %s)',msgtext,...
                    cause_i.identifier,cause_i.message);
            end
            
            msgtext = PreHEAT_API.ApiConfig.removeSensitiveContentFromString(...
                [msgtext, sprintf('\n \n Details: %s',error2use.message)] );
            
            ME = MException(msgID,msgtext);
            throw(ME);
        end
        
        function releaseRideThrough(n_trial,last_error_diagnostic)
            
            if PreHEAT_API.ApiConfig.onlyFastOperationAllowed()
                % Not allowing pause in automatic operation (to avoid
                % disruption)
                return;
            end
            
            release_duration_s = PreHEAT_API.ApiConfig.maxReleaseTolerance;
            
            % For the last trial, wait some minutes if the last failure was a timeout
            if ~isempty(last_error_diagnostic) && ...
                    n_trial<=PreHEAT_API.ApiConfig.retryIfFails && ...
                    PreHEAT_API.ApiConfig.exceptionWasTimeout(last_error_diagnostic)
                
                old_pause_status = pause('on');
                warning('The server was unavailable (automatically retrying in %.f seconds)', release_duration_s);
                pause(release_duration_s);
                pause(old_pause_status);
            end
            
        end
        
        function [is_timeout] = exceptionWasTimeout(exception2use)
            
            switch exception2use.identifier
                case {'PreHEAT:API:timeout','MATLAB:webservices:Timeout','MATLAB:webservices:HTTP503StatusCodeError'}
                    is_timeout = 1;
                    
                otherwise
                    is_timeout = 0;
            end
        end
        
        function [is_internal] = isRunningOnNeogridsInternalNetwork()
            running_in_internal_mode = getenv(PreHEAT_API.ApiConfig.envRunningOnNeogridsNetwork);
            is_internal = ~isempty(running_in_internal_mode)  && strcmp(running_in_internal_mode,'1');
        end        
    end
end