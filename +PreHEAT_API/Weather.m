classdef Weather < PreHEAT_API.Unit
    %Weather This class describes a weather unit (from weather
    %forecast/service.
    %   
    % This Class is a handle class and extends PreHEAT_API.Unit
    
    properties
        % Inherited from PreHEAT_API.Unit 
    end
    
    methods (Access='public')
        function obj = Weather(location_id, struct_unit)
            % Constructor creating a Weather object out of a structure with following fields
            %
            % Inputs:
            %       locationId  	Structure    
            %       struct_unit     Structure containing the relevant fields
            %                           for the desired object create among:     
            %                               id                  Name of the unit (string)
            %                               name                Name of the weather data (string)
            %                               types               Array of id and names for the weather data (struct(id,name) array)
            %
            
            obj.locationId  = location_id;                        
            obj.name        = 'weatherForecast';
            obj.type        = 'weather';           
                        
            obj.creationStruct = struct_unit;
            
            fields_struct_unit = fieldnames(struct_unit);
            
            for i=1:length(fields_struct_unit)                
                field_i = fields_struct_unit{i};
             	
                switch field_i                    
                    case 'gridId'
                        obj.id	= struct_unit.gridId;
                        
                    case 'types'
                        % Structure the fields in the appropriate components
                        for j=1:length(struct_unit.types)
                            type_j = struct_unit.types(j).name;
                            obj.addComponent(struct_unit.types(j), type_j );
                        end
                        
                    otherwise
                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                            sprintf('Loading of field "%s" is not yet supported.',field_i),...
                            'Weather' );
                end
            end
            
            inputStruct = struct('locationId',location_id,'weatherDetails',obj.getAllComponentDetails() );
            obj.data        = PreHEAT_API.Data( inputStruct );            
        end
        
        function loadData(obj,start_date, end_date, time_resolution, options2use)
            % Method to load data for all weather data over a given period.
            %
            % Inputs:
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of the current object
            %
                       
            if nargin<5
                options2use = struct();
            elseif ~isstruct(options2use)
                error('Input "options2use" must be a struct');
            end
            
            % Collects component ids and name of signals               
            if ~isfield(options2use, 'limitComponents') 
                options2use.limitComponents = '*';
            end
            component_details = obj.selectFocusComponents( options2use.limitComponents );
            
            if strcmp(time_resolution,'minute') || strcmp(time_resolution,'5min') || strcmp(time_resolution,'raw')
                time_resolution_API = 'hour'; 
            else
                time_resolution_API = time_resolution;
            end
            
            if ~isempty(component_details)
                % Loads the weather data
                obj.data.refreshComponentNames(component_details,'weather_data');
                obj.data.loadData(start_date,end_date,time_resolution_API );
                
                if strcmp(time_resolution,'minute') || strcmp(time_resolution,'5min')
                    obj.data.resample('minute',start_date,end_date);
                end
            end
        end        
                
        function addComponent(obj,component_details, component_name )
                        
            if isempty(component_details)
                obj.components.(component_name) = PreHEAT_API.WeatherComponent( [] );
                
            elseif isa(component_details,'struct')
                obj.components.(component_name) = PreHEAT_API.WeatherComponent(...
                    component_details, obj.locationId, component_name );
                
            elseif isa(component_details,'PreHEAT_API.WeatherComponent')
                obj.components.(component_name) = component_details;
                
            else
                error('Input "component_details" must be a struct or a PreHEAT_API.WeatherComponent object.');
            end            
        end
                
        function updateState(obj, time_resolution, update_time, expiry_limit)
            %updateState Updates the state of a component
            %
            % Valid calls:
            %   updateState(obj)
            %   updateState(obj, time_resolution)
            %   updateState(obj, time_resolution, update_time)
            %   updateState(obj, time_resolution, update_time, expiry_limit)
            %
            
            if (isempty(obj.id) || isnan(obj.id)) && (isempty(obj.cid) || isnan(obj.cid))
                return;
            end
            
            if nargin<2
                time_resolution = 'hour';
            end
            if nargin<3
                update_time = datetime('now','TimeZone','Europe/Copenhagen');
            end
            if nargin<4
                expiry_limit = hours(1);
            end            
            start_date  = update_time - expiry_limit;
            
            all_wtids    = obj.getAllWeatherTypeIds();
            data_table  = PreHEAT_API.Data.fetchWeatherData(obj.locationId,all_wtids.id, start_date, update_time, time_resolution);
            
            obj.importState(data_table);
        end
    end
    
    methods (Access='protected')       
        function [copy_obj] = copyElement(obj)
            copy_obj = copyElement@PreHEAT_API.Unit(obj);            
        end
    end
    
    methods (Access='private')
       % To be removed later (suspected that not used)
       function all_cids = getAllWeatherTypeIds(obj,add_prefix)                        
            if nargin<2
               add_prefix = false; 
            end            
            all_cids = obj.getAllComponentIds(add_prefix);
        end
        
        function componentDetails = getAllWeatherTypeDetails(obj,add_prefix)
            if nargin<2
               add_prefix = false; 
            end            
            componentDetails = obj.getAllComponentDetails(add_prefix);
        end         
    end
    
    methods (Static, Access='protected')
        
       function [focus_component_details] = isolateFocusComponents(component_details, focus_components)
            
            % If not having a specific list of focus components
            if iscell(focus_components) && length(focus_components)==1 && ...
                    strcmp(focus_components{1},'weather.*')
                focus_components = '*';
            end
            
            focus_component_details = isolateFocusComponents@PreHEAT_API.Unit(component_details, focus_components);
        end 
    end
end

