classdef Data < matlab.mixin.Copyable
    %Data Class to represent the PreHEAT data
    %
    % This class can be accessed by reference (inherits from handle) and copied (inherits from matlab.mixin.Copyable)
    
    properties (GetAccess='public', SetAccess='protected')
        data                = [];                       % Data (Matlab timetable)
        timezone            = 'UTC';                    % Timezone of the data (char)
        resolution          = '';                       % Time resolution of the data (char)
        timestep            = seconds(NaN);             % Timestep of the data (Matlab duration)
    end
    
    properties (GetAccess='protected', SetAccess='protected')
        startTime           = NaT('TimeZone','Europe/Copenhagen');  % Start time of the data (Matlab datetime)
        endTime             = NaT('TimeZone','Europe/Copenhagen');  % End time of the data (Matlab datetime)
        
        weatherDetails      = [];                       % table linking weather type ids to the details of their structure
        componentDetails    = [];                       % table box type ids to the details of their structure
    end
    
    methods (Access='public')
        function obj = Data( inputStruct )
            % Constructor creating a Data object out of a structure with
            % following fields. The data is then loaded from the database
            %
            % Inputs:
            %       inputStruct Structure containing the relevant fields
            %                       for the desired object create among:
            %                           locationId      Id of the location
            %                           weatherTypeIds  containers.Map (keys: type ids of the weather data & values: signal names) or [])
            %                           componentIds:	containers.Map (keys: component ids of the signals & values: signal names) or [])
            %                           resolution      Time resolution of the measurements to load
            %                           startDate       Start date of the measurements (in datetime format)
            %                           endDate         End date of the measurements (in datetime format)
            
            if nargin==0
                % By default, leave all parameters to default.
                
            elseif nargin==1 && isstruct(inputStruct)
                if isfield(inputStruct,'weatherDetails') && ~isempty(inputStruct.weatherDetails)
                    obj.refreshComponentNames(inputStruct.weatherDetails, 'weather_data');
                end
                if isfield(inputStruct,'componentDetails') && ~isempty(inputStruct.componentDetails)
                    obj.refreshComponentNames(inputStruct.componentDetails, 'box_data');
                end
                
            else
                error('[PreHEAT_API.Data] Illegal call to the constructor.');
            end
            
            % Creating the names dictionaries
        end
        
        function importData(obj, data_table, component_table, start_date, end_date, time_resolution, data_type)
            % Method to import data from a large data table.
            %
            % Inputs:
            %       data_table      Table containing the measurement data (columns: id,epochtime,value)
            %       component_table Table containing all component details (columns: id, name, stdUnitDivisor, stdUnit)
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of the current object
            %
            
            if nargin<7
                data_type = 'component';
            end
            
            switch data_type
                case {'component','box_data'}
                    type_for_resolution_check = 'box_data';
                    
                case {'weather','weather_data'}
                    type_for_resolution_check = 'weather_data';
                    
                otherwise
                    error('Unknown data type (%s)', data_type);
            end
            
            if obj.detectDoubleLoading(component_table,start_date,end_date,time_resolution,type_for_resolution_check) || ...
                    isempty(data_table)
                % Nothing to do
                return;
            end
            
            dates_OK   	= obj.checkInputDates(start_date, end_date);
            
            resolution_data	= obj.checkTimeResolution(time_resolution, type_for_resolution_check);
            obj.refreshComponentNames(component_table, type_for_resolution_check);
            
            if ~dates_OK
                % Do nothing
                data_timetable      = [];
                
            else
                % Checking validity of the input data
                obj.setTimeResolution(resolution_data);
                
                % Checking validity of the input dates
                [start_date,end_date] = obj.correctStartEndDate(start_date,end_date);
                
                % Adjusting timezone and creating the base of the timetable
                start_date.TimeZone	= obj.timezone;
                end_date.TimeZone	= obj.timezone;
                data_timetable      = obj.createBaseTimetable(start_date, end_date);
                
                some_data_was_loaded = 0;
                
                % Detecting which value field to use, and whether to
                % normalise data to standard unit
                if ismember('normalised_value',data_table.Properties.VariableNames)
                    value_field    = 'normalised_value';
                    normalise_data = 0;
                elseif ismember('value',data_table.Properties.VariableNames)
                    value_field    = 'value';
                    normalise_data = 1;
                else
                    error('Values not found in the data table ');
                end
                
                component_table_field = 'id';
                if ismember('id',data_table.Properties.VariableNames)
                    id_field = 'id';
                elseif ismember('type_id',data_table.Properties.VariableNames)
                    id_field = 'type_id';
                elseif ismember('box_component_id',data_table.Properties.VariableNames)
                    id_field = 'box_component_id';
                    component_table_field = 'cid';
                else
                    error('Values not found in the data table ');
                end
                
                
                if ~isempty(component_table) && ~isempty(data_table)
                    
                    unique_cids = unique( component_table{:,component_table_field} );
                    
                    for i=1:length(unique_cids)
                        
                        if ismember('epochtime',data_table.Properties.VariableNames)
                            T_i = data_table( data_table{:,id_field}==unique_cids(i), {'epochtime',value_field} );
                            time_type_i = 'epochtime';
                        elseif ismember('time',data_table.Properties.VariableNames)
                            T_i = data_table( data_table{:,id_field}==unique_cids(i), {'time',value_field} );
                            time_type_i = 'time';
                        else
                            error('Time not included in data.');
                        end
                        
                        component_details_i = component_table(component_table{:,component_table_field}==unique_cids(i),:);
                    
                        if strcmp(time_type_i,'epochtime')
                            times_T_i = timetable(datetime( T_i.epochtime, 'ConvertFrom', 'epochtime', 'TimeZone', 'UTC' ));
                        elseif strcmp(time_type_i,'time')
                            times_T_i = timetable(T_i.time);
                        else
                            error('Invalid date/time format');
                        end
                        
                        for j=1:height(component_details_i)
                            name2use_ij = component_details_i.type{j};
                            
                            if regexp(name2use_ij,'[0-9]','once')==1
                                name2use_ij = strcat('c',name2use_ij);
                            end
                            name2use_ij = strrep(name2use_ij,' ','_');
                            
                            % Creating the timetable
                            temp_ij = times_T_i;
                            if normalise_data
                                temp_ij{:,name2use_ij} = T_i{:,value_field} / component_details_i.stdUnitDivisor(j);
                            else
                                temp_ij{:,name2use_ij} = T_i{:,value_field};
                            end
                            
                            % Adding the unit
                            if ismember('stdUnit',component_details_i.Properties.VariableNames) && ~isempty(component_details_i.stdUnit{j}) && ...
                                    ~strcmp(component_details_i.stdUnit{j},'?')
                                temp_ij.Properties.VariableUnits = component_details_i.stdUnit(j);
                                
                            elseif ismember('unit',component_details_i.Properties.VariableNames) && ~isempty(component_details_i.unit{j})
                                temp_ij.Properties.VariableUnits = component_details_i.unit(j);
                                
                            else
                                temp_ij.Properties.VariableUnits = {'?'};
                            end
                            
                            if isempty(temp_ij)
                                % Do nothing (to avoid adding a column of NaNs)
                            elseif strcmp(obj.resolution,'raw')
                                % For raw data, just make a union of all
                                % times available
                                temp_ij.Properties.DimensionNames{1} = 't';
                                temp_ij.t.TimeZone = 'UTC';
                                
                                if isempty(data_timetable.Properties.VariableNames)
                                    data_timetable = temp_ij;
                                else
                                    data_timetable = PreHEAT_API.Data.synchroniseData(data_timetable, temp_ij, 'union');
                                end
                                some_data_was_loaded = 1;
                            else
                                % Else, synchronise everything with the
                                % reference timetable
                                data_timetable = PreHEAT_API.Data.synchroniseData(data_timetable, temp_ij, 'first');
                                some_data_was_loaded = 1;
                            end
                        end
                    end
                end
            end
            
            if some_data_was_loaded
                % Adding the data to the current object data
                obj.addData(data_timetable,resolution_data);
            end
        end
        
        function loadData(obj, start_date, end_date, time_resolution)
            
            % Checking validity of the input dates
            obj.setTimeResolution( time_resolution );
            [start_date,end_date] = obj.correctStartEndDate(start_date,end_date);
            
            % Loading weather if relevant
            if ~isempty(obj.weatherDetails)
                obj.loadWeatherData( ...
                    obj.weatherDetails,     ...
                    start_date,             ...
                    end_date,               ...
                    time_resolution );
            end
            
            % Loading box data if relevant
            if ~isempty(obj.componentDetails)
                obj.loadBoxData(...
                    obj.componentDetails, 	...
                    start_date,             ...
                    end_date,               ...
                    time_resolution );
            end
        end
        
        function mergeData(obj, extra_data)
            
            if isempty(extra_data)
                % Nothing to do
                return;
            end
            
            if isempty(obj)
                obj.data        = extra_data.data([],:);
                obj.timezone 	= extra_data.timezone;
                obj.resolution	= extra_data.resolution;
                obj.timestep 	= extra_data.timestep;
                
            elseif ~strcmp(obj.resolution,extra_data.resolution)
                error('Cannot merge data with two different timesteps');
            end
            
            % Add missing columns in current and new data table, if any
            missing_columns_names_obj = setdiff(extra_data.data.Properties.VariableNames,...
                obj.data.Properties.VariableNames);
            if ~isempty(missing_columns_names_obj)
                % Add columns
                obj.data{:,missing_columns_names_obj} = NaN(height(obj.data),length(missing_columns_names_obj));
                
                % Add component names
                if ~isempty(extra_data.componentDetails)
                    if isempty(obj.componentDetails)
                        component_table = extra_data.componentDetails;
                    else
                        component_table = union(obj.componentDetails,extra_data.componentDetails);
                    end
                    obj.refreshComponentNames(component_table, 'box_data');
                end
                
                if ~isempty(extra_data.weatherDetails)
                    if isempty(obj.componentDetails)
                        weather_table   = extra_data.weatherDetails;
                    else
                        weather_table   = union(obj.weatherDetails,  extra_data.weatherDetails);
                    end
                    obj.refreshComponentNames(weather_table,   'weather_data');
                end
            end
            
            extra_data_table = extra_data.data;
            missing_columns_names_new = setdiff(obj.data.Properties.VariableNames,...
                extra_data_table.Properties.VariableNames);
            if ~isempty(missing_columns_names_new)
                extra_data_table{:,missing_columns_names_new} = NaN(height(extra_data_table),length(missing_columns_names_new));
            end
            
            % Check if the data was already merged
            if all(ismember(extra_data_table.t,obj.data.t))
                % Nothing to do, as the data should already be merged
            else
                % Merge the columns
                merged_column_names = obj.data.Properties.VariableNames;
                obj.data = union(obj.data(:,merged_column_names),extra_data_table(:,merged_column_names));
                obj.updateStartEndTime();
            end
        end
        
        function removeData(obj, criterion, detail)
            
            if isempty(obj)
                return; % Nothing to do on empty data
            end
            
            need_to_update_start_end_time = 0;
            
            switch criterion
                case 'before'
                    if ~isdatetime(detail) || isempty(detail.TimeZone)
                        error('Input "detail" must be a datetime with timezone');
                    end
                    obj.data(obj.data.t<detail,:) = [];
                    need_to_update_start_end_time = 1;
                    
                case 'after'
                    if ~isdatetime(detail) || isempty(detail.TimeZone)
                        error('Input "detail" must be a datetime with timezone');
                    end
                    obj.data(obj.data.t>detail,:) = [];
                    need_to_update_start_end_time = 1;
                    
                otherwise
                    error('Unknown use case for criterion (%s)',criterion);
            end
            
            if need_to_update_start_end_time
                obj.updateStartEndTime();
            end
        end
        
        function component_is_in_data = containsComponent(obj, component_name)
            % Method loading sensor data from a start to an end date with a chosen time resolution
            %
            % Input:
            %       component_name          Component identifier to look for (either integer or char, PreHEAT_API.Component id or name)
            %                                   OBS: here weather type id are not considered,
            %                                       but weather data can be accessed by name.
            % Output:
            %       component_is_in_data    Returns whether the component
            %       is part of the data (true) or isn't (false) (boolean)
            %
            if isempty(obj.data)
                component_is_in_data = 0;
                
            elseif ischar(component_name) || iscell(component_name)
                % Looking for a component name
                
                % Fetching all component names
                all_identifiers = obj.data.Properties.VariableNames;
                
                % Checks whether the desired component is into the dataset
                component_is_in_data = ismember(component_name,all_identifiers);
                
            else
                error('Argument component_name is of invalid type (only char is supported).');
            end
            
        end
        
        function component_details = getComponentDetails(obj)
            component_details   = obj.componentDetails;
        end
        
        function weather_details = getWeatherDetails(obj)
            weather_details     = obj.weatherDetails;
        end
        
        function [units_table] = getDataUnits(obj)
            
            if isempty(obj.data)
                signal  = {};
                unit    = {};
            else
                signal  = obj.data.Properties.VariableNames';
                unit    = obj.data.Properties.VariableUnits';
            end
            units_table = table(signal,unit);
        end
        
        function [start_time] = getStartTime(obj,timezone2use)
            if nargin<2
                timezone2use = 'Europe/Copenhagen';
            end
            start_time          = obj.startTime;
            start_time.TimeZone = timezone2use;
        end
        
        function [end_time] = getEndTime(obj,timezone2use)
            if nargin<2
                timezone2use = 'Europe/Copenhagen';
            end
            end_time          = obj.endTime;
            end_time.TimeZone = timezone2use;
        end
        
        function clearData(obj)
            % Reinitialises the object to the default values
            obj.data        = [];
            obj.timezone    = 'UTC';
            obj.resolution  = '';
            obj.timestep    = seconds(NaN);
            obj.startTime   = NaT('TimeZone','Europe/Copenhagen');
            obj.endTime     = NaT('TimeZone','Europe/Copenhagen');
        end
        
        function resample(obj, time_resolution, start_time, end_time)
            % Resamples the time vector (filling gaps with missing data markers)
            
            if isempty(obj.data)
                return;
            end
            
            if nargin<3
                start_time = obj.getStartTime(obj.timezone);
            end
            
            if nargin<4
                end_time   = obj.getEndTime(obj.timezone);
            end
            
            switch time_resolution
                case 'minute'
                    dt = minutes(5);
                case 'hour'
                    dt = hours(1);
                case 'day'
                    dt = caldays(1);
                case 'week'
                    dt = calweeks(1);
                case 'month'
                    dt = calmonths(1);
                case 'year'
                    dt = calyears(1);
                otherwise
                    error('Unsupported time resolution (%s)',time_resolution);
            end
            
            % Adjusting timezone of new sample times
            new_times   = [start_time:dt:end_time-seconds(1)]';
            if isempty(new_times.TimeZone)
                new_times.TimeZone = obj.data.startDate(1).TimeZone;
            end
            
            % Resampling the data (dates are handled separately)
            fields_non_times = setdiff(obj.data.Properties.VariableNames,{'startDate','endDate'});
            obj.data    = retime(obj.data(:,fields_non_times),new_times,'fillwithmissing');
            obj.data.startDate  = obj.data.t;
            obj.data.endDate    = obj.data.startDate + dt - seconds(1);
            
            obj.setTimeResolution(time_resolution);
            obj.updateStartEndTime();
        end
        
        function addData(obj,data_to_add,time_resolution)
            % Method loading sensor data from a start to an end date with a chosen time resolution
            %
            % Input:
            %       data_to_add 	Data to add to the attribute data (PreHEAT_API.Data)
            %       time_resolution Time resolution of the data (char, either 'raw','minute' or '5min','hour','day','week','month','year')
            %
            % Output:
            %       /               The data_to_add is added into the data attribute
            %                           and startDate, endDate, and resolution attributes are updated
            %                           (PreHEAT_API.Data)
            %
            if nargin<2
                error('Argument "data_to_add" must be specified');
            end
            if nargin<3
                error('Argument "time_resolution" must be specified');
            end
            
            if isempty(data_to_add)
                % Nothing to do
                return;
            elseif istimetable(data_to_add)
                % Nothing to do.
            else
                % Else the input is a PreHEAT_API.Data, which data must be
                % extracted.
                data_to_add = data_to_add.data;
            end
            
            
            if isempty(data_to_add)
                % If the data to add is empty, skip the work
                return;
            elseif isempty(obj.data)
                % If the object does not contain data, just use the new one
                obj.data = data_to_add;
            else
                % If data is already available, then merging is made using
                % union of both times
                data_fields = setdiff(data_to_add.Properties.VariableNames,{'startDate','endDate'});
                obj.data = PreHEAT_API.Data.synchroniseData( obj.data, data_to_add(:,data_fields), 'union' );
            end
            
            % Updating the object attributes
            if nargin<3
                obj	= obj.setTimeResolution(time_resolution);
            end
            obj.updateStartEndTime();
        end
        
        function [data_is_empty] = isempty(obj)
            
            if length(obj)==0
                data_is_empty = 1;
            else
                data_is_empty = isempty(obj.data);
            end
        end
        
        function refreshComponentNames(obj, component_table, type)
            
            switch type
                case 'box_data'
                    obj.componentDetails = component_table;
                    
                case 'weather_data'
                    obj.weatherDetails = component_table;
                    
                otherwise
                    disp(type);
                    error('Illegal "type" value %s');
            end
        end
        
        function summary(obj)
            fprintf('Timestep:   %s \n',char(obj.timestep));
            fprintf('TimeZone:   %s \n',obj.timezone);
            fprintf('Start time: %s \n',datestr(obj.startTime));
            fprintf('End time:   %s \n',datestr(obj.endTime));
            summary(obj.data);
        end
    end
    
    methods (Access='protected')
        function [resolution_data] = checkTimeResolution(obj,time_resolution, usage)
            % Method checking that the time resolution asked for is appropriate for the desired usage
            %
            % Input:
            %       time_resolution     Time resolution to use (char, either 'raw','minute' or '5min','hour','day','week','month','year')
            %       usage               Type of data considered (char, either 'weather_data' or 'box_data')
            %
            % Output:
            %       resolution_data     Valid name of the resolution to use (char, either 'raw','minute' or '5min','hour','day','week','month','year')
            %
            % Error:
            %       Throws an error if the time_resolution is not valid.
            
            switch usage
                case {'weather_data','box_data'}
                    % Usage is allowed
                otherwise
                    error('Illegal usage (%s).',usage)
            end
            
            switch time_resolution
                case 'raw'
                    if strcmp(usage,'weather_data')
                        resolution_data     = 'hour';
                    else
                        resolution_data     = 'raw';
                    end
                    
                case {'5min','minute'}
                    if strcmp(usage,'weather_data')
                        resolution_data     = 'hour';
                    else
                        resolution_data     = 'minute';
                    end
                    
                case 'hour'
                    resolution_data     = 'hour';
                case 'day'
                    resolution_data     = 'day';
                case 'week'
                    resolution_data     = 'week';
                case 'month'
                    resolution_data     = 'month';
                case 'year'
                    resolution_data     = 'year';
                otherwise
                    error('Unknown timestep (%s)', time_resolution );
            end
            
            % Check that there is not change of timestep between the
            % requested data and the current one
            if ~isempty(obj) &&  ~isempty(obj.resolution) && ~strcmp(obj.resolution,resolution_data)
                % Return an error if one is trying to mix different
                % time resolutions
                error('The chosen timestep (%s) should be the same as the timestep of the current data (%s).',...
                    resolution_data, obj.resolution );
            end
            
        end
        
        function [start_date_to_API,end_date_to_API] = correctStartEndDate(obj,start_date,end_date)
            
            start_date_to_API	= start_date;
            end_date_to_API 	= end_date;
            
            % Forces the timezone of the dates to local where relevant (to
            % match API conventions)
            switch obj.resolution
                case {'5min','minute','day','week','month','year'}
                    % For these cases, the start of the day considered is
                    % always in local time.
                    start_date_to_API.TimeZone  = 'Europe/Copenhagen';
                    end_date_to_API.TimeZone    = 'Europe/Copenhagen';
                otherwise
                    % Nothing to do
            end
            
            % Forces the dates to be exactly the ones from the DB
            margin_end_date = seconds(2); % Adding 2 seconds to be robust against the use of a 1 second shift
            switch obj.resolution
                
                case {'5min','minute'}
                    % Adjusting for the closest 5 minutes timestep
                    period_min	= 5;
                    start_hour  = dateshift(start_date_to_API,'start','hour');
                    start_date_to_API   = dateshift(start_date_to_API,              'start','minute');
                    end_date_to_API     = dateshift(end_date_to_API-margin_end_date,'end',  'minute');
                    start_date_to_API   = start_hour + minutes( period_min * floor(minutes(start_date_to_API - start_hour)/period_min) );
                    end_date_to_API     = start_hour + minutes( period_min * ceil(minutes(end_date_to_API-margin_end_date - start_hour)/period_min) );
                    
                    
                case {'hour','day','month','year'}
                    start_date_to_API   = dateshift(start_date_to_API,              'start',obj.resolution);
                    end_date_to_API     = dateshift(end_date_to_API-margin_end_date,'end',obj.resolution);
                    
                case 'week'
                    offset_time         = caldays(1); % Matlab starts week on Sunday, the database starts on monday.
                    start_date_to_API   = dateshift(start_date_to_API,              'start','week') + offset_time;
                    end_date_to_API     = dateshift(end_date_to_API-margin_end_date,'end','week') + offset_time;
                    
                otherwise
                    % Nothing to do
            end
        end
        
        function [dates_OK] = checkInputDates(obj,start_date, end_date)
            % Method checking that the time resolution asked for is appropriate for the desired usage
            %
            % Input:
            %       start_date      Start time of the data (Matlab datetime)
            %       end_date        End time of the data (Matlab datetime)
            %
            % Output:
            %       dates_OK        Verification that the days are valid (true) or bad (false)
            %
            % Error:
            %       Throws an error if the dates are not valid.
            %
            
            if isnat(start_date)
                error('NaT value not allowed for start date');
            elseif isnat(end_date)
                error('NaT value not allowed for end date');
            elseif end_date<start_date
                error('Start date (%s) must be inferior to end date (%s)',datestr(start_date),datestr(end_date));
            else
                dates_OK = 1;
            end
        end
        
        function setTimeResolution(obj,time_resolution)
            % Method checking that the time resolution asked for is appropriate for the desired usage
            %
            % Input:
            %       time_resolution     Time resolution to use (char, either 'raw','minute' or '5min','hour','day','week','month','year')
            %
            % Output:
            %       /                   Sets the attribute time_resolution of the current object
            % Error:
            %       Throws an error if the time_resolution is not valid.
            
            switch time_resolution
                case 'raw'
                    obj.resolution      = 'raw';
                case {'5min','minute'}
                    obj.resolution      = 'minute';
                case 'hour'
                    obj.resolution      = 'hour';
                case 'day'
                    obj.resolution      = 'day';
                case 'week'
                    obj.resolution      = 'week';
                case 'month'
                    obj.resolution      = 'month';
                case 'year'
                    obj.resolution      = 'year';
                otherwise
                    error('Unknown resolution (%s)', time_resolution );
            end
            
            approximate_for_division = 0;
            obj.timestep = PreHEAT_API.Data.convertResolutionNameToDuration(obj.resolution, approximate_for_division);
        end
        
        function [base_timetable] = createBaseTimetable(obj, start_date, end_date)
            % Method to create a base timetable to structure the data
            %
            % Input:
            %       start_date          Start date of the data (Matlab datetime)
            %       end_date            End date of the data (Matlab datetime)
            %
            % Output:
            %       base_timetable     	Basic timetable (Matlab timetable, with time t, and columns startDate and endDate)
            %
            
            if strcmp(obj.resolution,'raw')
                t = [start_date;end_date];
                base_timetable	= timetable(t);
            else                
                start_date.TimeZone = 'Europe/Copenhagen';
                startDate	= [start_date:obj.timestep:(end_date-seconds(1))]';
                t        	= startDate;  
                endDate  	= startDate+obj.timestep-seconds(1);         
                base_timetable      = timetable(t,startDate,endDate);
            end
            
            % Start and end date are converted in local timezone, t remains in UTC
            base_timetable.t.TimeZone = 'UTC';
        end
        
        function double_loading_occurring = detectDoubleLoading(obj,component_table,start_date,end_date,time_resolution,data_type)
            % Detecting reloading of the data if is is already loaded
            double_loading_occurring = 0;
            
            if ~isempty(obj)
                start_time_loaded   = obj.getStartTime(start_date.TimeZone);
                end_time_loaded     = obj.getEndTime(end_date.TimeZone);
                
                data_has_same_start_end = ...
                    abs(start_time_loaded-start_date)<minutes(1) && ...
                    abs(end_date-end_time_loaded)<minutes(1);
                
                
                if data_has_same_start_end && strcmp(time_resolution,obj.resolution)
                    
                    switch data_type
                        case 'box_data'
                            data_has_same_components = all(ismember( component_table.id, obj.componentDetails.id ));
                        case 'weather_data'
                            data_has_same_components = all(ismember( component_table.id, obj.weatherDetails.id ));
                        otherwise
                            error('Illegal "data_type"');
                    end
                    
                    % No need to compute further
                    double_loading_occurring = data_has_same_components;
                end
            end
        end
        
        function loadWeatherData(obj, weather_types_table, start_date, end_date, time_resolution )
            % Method loading the weather data from a start to an end date with a chosen time resolution
            %
            % Input:
            %       weatherTypeIds      Type ids and names of the parameters to read (containers.map with keys=typeId and value=parameterName)
            %
            %       start_date          Start date of the data to load (in datetime format)
            %       end_date            End date of the data to load (in datetime format)
            %       time_resolution     Time resolution (char, either 'raw','minute' or '5min','hour','day','week','month','year')
            %
            % Output:
            %       /                   The data is loaded in the attribute 'data' in format PreHEAT_API.Data
            %
            
            % Using median in case some locationIds are missing
            location_id = median(weather_types_table.locationId,'omitnan');
            
            if obj.detectDoubleLoading(weather_types_table,start_date,end_date,time_resolution,'weather_data')
                % Nothing to do
                return;
            end
            
            % Loading the data from the API
            [data_table] = obj.fetchWeatherData(location_id, weather_types_table.id, start_date,end_date,time_resolution);
            
            % Importing the data
            obj.importData(data_table, weather_types_table, start_date, end_date, time_resolution, 'weather_data');
        end
        
        function loadBoxData(obj, component_table, start_date, end_date, time_resolution )
            % Method loading sensor data from a start to an end date with a chosen time resolution
            %
            % Input:
            %       componentDetails    Component ids and names of the parameters to read
            %                               (containers.map with keys=componentId and value=struct containing the name, stdUnitDivisor, and stdUnit of the components)
            %
            %       start_date          Start date of the data to load (in datetime format)
            %       end_date            End date of the data to load (in datetime format)
            %       time_resolution     Time resolution (char, either 'raw','minute' or '5min','hour','day','week','month','year')
            %
            % Output:
            %       /                   The data is loaded in the attribute 'data' in format PreHEAT_API.Data
            %
            
            if obj.detectDoubleLoading(component_table,start_date,end_date,time_resolution,'box_data')
                % Nothing to do
                return;
            end
            
            % Loading the data from the API
            [data_table] = obj.fetchComponentData(component_table.id, start_date,end_date,time_resolution, 'id');
            
            % Importing the data
            obj.importData(data_table, component_table, start_date, end_date, time_resolution,'box_data');
        end
        
        function updateStartEndTime(obj)
            if isempty(obj)
                obj.clearData();% Nothing else than reset the data when it is empty
                return;
            end
            if obj.containsComponent('startDate')
                obj.startTime	= obj.data.startDate(1);
            else
                obj.endTime     = min(obj.data.t);                
            end
            if obj.containsComponent('endDate')
                obj.endTime	= obj.data.endDate(end);
            else
                obj.endTime	= max(obj.data.t);                
            end
            obj.startTime.TimeZone  = 'Europe/Copenhagen';
            obj.endTime.TimeZone    = 'Europe/Copenhagen';
        end
    end
    
    methods (Static, Access='public')
        
        function [T_merged] = synchroniseData(T1,T2, method2use)
            
            % Synchronise the variables
            T_merged = synchronize(T1,T2,method2use);
            
            % Merge duplicate variable names
            varnames = T_merged.Properties.VariableNames;
            k_duplicate = find(endsWith(varnames,'_T1'));
            for i=1:length(k_duplicate)
                merged_var = varnames{k_duplicate(i)}(1:(end-length('_T1')));
                vars_2_merge = { strcat(merged_var,'_T1'),strcat(merged_var,'_T2') };
                T_merged = mergevars(T_merged,vars_2_merge,'NewVariableName', merged_var);
            end
        end
        
        function [data_table] = fetchComponentData(component_ids, varargin)
            %fetchComponentData     Fetches data for specific component ids
            %
            % Valid calls
            %   [data_table] = fetchComponentData(component_ids, start_date, end_date, time_resolution, fetch_by)
            %   [data_table] = fetchComponentData(component_ids, fetch_by)
            %   [data_table] = fetchComponentData(component_ids, fetch_by, time_resolution)
            %
            
            if isempty(component_ids)
                % Short circuiting execution if not components are used
                data_table = [];
                return;
            end
            
            % If calling with too many components, re-call on several
            % instances and collate outputs
            N_components = length(component_ids);
            N_max_components_per_call = PreHEAT_API.ApiConfig.apiMaxCidsPerCall;            
            if N_components>N_max_components_per_call
                
                data_table = [];
                
                for i=1:N_max_components_per_call:N_components
                    k_i = [i:1:min(N_components,i+(N_max_components_per_call-1))]';
                    components2load_i = component_ids(k_i);
                    data_table_i = PreHEAT_API.Data.fetchComponentData(...
                        components2load_i, varargin{:});
                    if ~isempty(data_table_i)
                        data_table   = [data_table ; data_table_i];
                    end
                end                
                return;
            end
            
            if nargin<=3 && ischar(varargin{1})
                % Special case, if only looking for first or last data
                fetch_by = varargin{1};
                
                switch fetch_by
                    case 'latest_id'
                        query4sprintf = 'units/measurements/latest?ids=%s&time_resolution=%s&timestamp_type=epoch_milliseconds';
                        if nargin<3
                            time_resolution = 'raw'; % By default, call on raw data
                        else
                            time_resolution = varargin{2}; 
                        end
                        
                    case 'earliest_id'
                        query4sprintf = 'units/measurements/earliest?ids=%s&time_resolution=%s&timestamp_type=epoch_milliseconds';
                        if nargin<3
                            time_resolution = 'minute'; % By default, call on 5min data
                        else
                            time_resolution = varargin{2}; 
                        end
                        
                    otherwise
                        error('Illegal value of parameter "fetch_by" (%s).',fetch_by);
                end
                
                % Serialising all the type ids for the components to
                % load in this iteration
                cids_box_data	= PreHEAT_API.ApiConfig.numericVectorToString( component_ids, ',');
                                
                % Calling the API (using "epoch_milliseconds" for
                % performance, rather than "iso_8601")
                data_table	= PreHEAT_API.ApiConfig.apiGet(...
                    sprintf(query4sprintf,cids_box_data,time_resolution),...
                    'csv');
                value_fieldname = 'normalised_value';
                
            else
                start_date      = varargin{1};
                end_date        = varargin{2};
                time_resolution = varargin{3};
                if nargin<5
                    fetch_by = 'cid';
                else
                    fetch_by = varargin{4};
                end
                
                
                PreHEAT_API.Data.checkValidResolution(time_resolution,'building');
                
                data_table = [];
                
                if ~isnumeric(component_ids)
                    error('Component ids must be a vector of integers.');
                elseif ~isrow(component_ids) && iscolumn(component_ids)
                    component_ids = component_ids';
                else
                    % Nothing to do
                end
                
                if ~ischar(time_resolution)
                    error('Time resolution must be a string.');
                elseif strcmp(time_resolution,'5min')
                    time_resolution = 'minute';
                else
                    % Nothing to do
                end
                
                % Converting the resolution to a Matlab duration
                dt_requested_exact = PreHEAT_API.Data.convertResolutionNameToDuration(...
                    time_resolution, 0);
                dt_requested = PreHEAT_API.Data.convertResolutionNameToDuration(...
                    time_resolution, 1);
                
                % Creating query string for sprintf based upon the chosen
                % fetching method
                switch fetch_by
                    case 'id'
                        query4sprintf = 'units/measurements?ids=%s&start_time=%s&end_time=%s&time_resolution=%s&timestamp_type=epoch_milliseconds';
                        value_fieldname = 'normalised_value';
                        
                    case 'cid' % Legacy function
                        % Override if a database connection is available
                        if PreHEAT_API.Data.neogridDatabaseConnectionIsAvailable()
                            load_struct = struct('type','box',...
                                'component_ids',component_ids,...
                                'start_date',start_date,'end_date',end_date,...
                                'time_resolution',time_resolution);
                            data_table = PreHEAT_API.Data.getDataFromNeogridDB(load_struct);
                            return;
                        end
                        query4sprintf = 'measurements?cids=%s&start_time=%s&end_time=%s&time_resolution=%s&timestamp_type=epoch_milliseconds';
                        value_fieldname = 'value';
                        
                    case 'latest_id'
                        query4sprintf = 'units/measurements/latest?ids=%s&start_time=%s&end_time=%s&time_resolution=%s&timestamp_type=epoch_milliseconds';
                        value_fieldname = 'normalised_value';
                        
                    case 'earliest_id'
                        query4sprintf = 'units/measurements/earliest?ids=%s&start_time=%s&end_time=%s&time_resolution=%s&timestamp_type=epoch_milliseconds';
                        value_fieldname = 'normalised_value';
                        
                    otherwise
                        error('Illegal value of fetch_by input (%s)',fetch_by);
                end
                
                % Loading the data per blocks of limited number of CIDs and time resolution at once
                max_points_per_call = PreHEAT_API.ApiConfig.apiMaxPointsPerCall;
                dt_max_per_component= (dt_requested_exact*max_points_per_call);
                
                for start_i=start_date:dt_max_per_component:end_date
                    
                    end_i = min( start_i+dt_max_per_component-seconds(1), end_date );
                    
                    N_points_per_component  = ( end_i - start_i ) / dt_requested;
                    
                    % Converting dates to API date format
                    start_date_API	= PreHEAT_API.ApiConfig.convertToApiInputDate( start_i );
                    end_date_API 	= PreHEAT_API.ApiConfig.convertToApiInputDate( end_i );
                    
                    N_max_cids_at_once      = floor( max_points_per_call / N_points_per_component );
                    N_total_cids            = length(component_ids);
                    
                    for i=1:N_max_cids_at_once:N_total_cids
                        
                        typeids_i       = component_ids( i:...
                            min( (i+N_max_cids_at_once-1), N_total_cids ) );
                        
                        % Serialising all the type ids for the components to
                        % load in this iteration
                        cids_box_data	= PreHEAT_API.ApiConfig.numericVectorToString( typeids_i, ',');
                        
                        
                        % Calling the API (using "epoch_milliseconds" for
                        % performance, rather than "iso_8601")
                        apiOutputStruct	= PreHEAT_API.ApiConfig.apiGet(...
                            sprintf(query4sprintf,...
                            cids_box_data,start_date_API,end_date_API,time_resolution),...
                            'csv');
                        
                        % Merging the API outputs into the data
                        if ~isempty(apiOutputStruct)
                            data_table   = [data_table ; apiOutputStruct];
                        end
                    end
                end                
            end
            
            % Updating time name to epoch time
            if ~isempty(data_table)
                if ~istable(data_table)
                    error('data_table is not a table');
                end
                if isnumeric(data_table.time)
                    % If the time column is milli-epoch
                    data_table.epochtime = data_table.time / 1000;
                    data_table.time = [];
                end
                % Adjusting the fieldname for values
                data_table.Properties.VariableNames{'value'} = value_fieldname;
            end
        end
        
        function [data_table] = fetchWeatherData(location_id,weather_type_ids, start_date, end_date, time_resolution)
            
            PreHEAT_API.Data.checkValidResolution(time_resolution,'weather');
                        
            % Creating query string for sprintf based upon the chosen fetching method
            query4sprintf = 'weather/%i?type_ids=%s&start_time=%s&end_time=%s&time_resolution=%s&timestamp_type=epoch_milliseconds';
            value_fieldname = 'normalised_value';
            
            
            % API call to get the weather data
            data_table = [];
            
            if ~isnumeric(weather_type_ids)
                error('Component ids must be a vector of integers.');
            elseif ~isrow(weather_type_ids) && iscolumn(weather_type_ids)
                weather_type_ids = weather_type_ids';
            else
                % Nothing to do
            end
            
            if ~ischar(time_resolution)
                error('Time resolution must be a string.');
            elseif strcmp(time_resolution,'5min')
                time_resolution = 'minute';
            else
                % Nothing to do
            end
            
            if strcmp(time_resolution,'hour')
                max_duration_at_once = calmonths(6);
            else
                max_duration_at_once = years(6);
            end
            
            for start_i = start_date:max_duration_at_once:end_date
                
                end_i = min(start_i+max_duration_at_once-seconds(1), end_date);
                
                % Converting dates to API date format
                start_date_API  = PreHEAT_API.ApiConfig.convertToApiInputDate( start_i );
                end_date_API    = PreHEAT_API.ApiConfig.convertToApiInputDate( end_i );
                
                % Serialising all the type ids for the weather
                typeids_weather = unique(weather_type_ids);
                cids_weather   	= PreHEAT_API.ApiConfig.numericVectorToString( typeids_weather, ',');
                
                % Calling the API (using "epoch_milliseconds" for
                % performance, rather than "iso_8601")
                apiOutputStruct = PreHEAT_API.ApiConfig.apiGet(...
                    sprintf(query4sprintf,location_id,cids_weather,start_date_API,end_date_API,time_resolution),...
                    'csv');
                
                % Merging the API outputs into the data
                if ~isempty(apiOutputStruct)
                    data_table   = [data_table ; apiOutputStruct];
                end
            end
            
            if ~isempty(data_table)
                if ~istable(data_table)
                    error('data_table is not a table');
                end
                if isnumeric(data_table.time)
                    % If the time column is milli-epoch
                    data_table.epochtime = data_table.time / 1000;
                    data_table.time = [];
                end
                % Adjusting the fieldname for values
                data_table.Properties.VariableNames{'value'} = value_fieldname;
            end
        end
    end
    
    methods (Static, Access='private')
        
        function [dt_requested] = convertResolutionNameToDuration(time_resolution, approximate_for_division)
            % Converting the resolution to a Matlab duration
            
            if nargin<2
                approximate_for_division = 0;
            end
            
            if isduration(time_resolution)
                dt_requested = time_resolution;
                
            elseif ~ischar(time_resolution)
                error("Argument 'time_resolution' must be a string.");
            end
            
            switch time_resolution
                case {'minute','5min'}
                    dt_requested = minutes(5);
                    
                case 'hour'
                    dt_requested = hours(1);
                    
                case 'day'
                    if approximate_for_division
                        dt_requested = days(1);
                    else
                        dt_requested = caldays(1);
                    end
                    
                case 'week'
                    if approximate_for_division
                        dt_requested = days(7);
                    else
                        dt_requested = calweeks(1);
                    end
                    
                case 'month'
                    if approximate_for_division
                        dt_requested = days(28);
                    else
                        dt_requested = calmonths(1);
                    end
                    
                case 'year'
                    if approximate_for_division
                        dt_requested = years(1);
                    else
                        dt_requested = calyears(1);
                    end
                    
                case 'raw'
                    % By convention, this is set to 30s
                    dt_requested = seconds(30);
                    
                otherwise
                    error("Illegal value of parameter 'time_resolution' (%s)",time_resolution);
            end
        end
        
        function [data_table] = convertApiStructToMeasurementTable(apiOutputStruct)
            
            data_table = table();
            
            % Merging the API outputs
            if ~isempty(fieldnames(apiOutputStruct))
                
                field_names_i = fieldnames(apiOutputStruct);
                
                for j=1:length(field_names_i)
                    field_names_i_j = field_names_i{j};
                    struct_i_j = apiOutputStruct.(field_names_i_j);
                    
                    T_i = table();
                    
                    if ~isempty(struct_i_j)
                        T_i_j = table();
                        if isfield(struct_i_j,'time') && ~isempty(struct_i_j)
                            if isnumeric(struct_i_j(1).time)
                                % If the time column is milli-epoch
                                T_i_j.epochtime = [struct_i_j.time]'/1000;
                            else
                                T_i_j.time = {struct_i_j.time}';
                            end
                        end
                        if isfield(struct_i_j,'value')
                            T_i_j.value = [struct_i_j.value]';
                        end
                        T_i_j{:,'id'}   = str2double(field_names_i_j(2:end)) * ones(height(T_i_j),1);
                        T_i     = [ T_i ; T_i_j ];
                    end
                    if ~isempty(T_i)
                        data_table = [data_table ; T_i];
                    end
                end
            end
        end
        
        function [connection_available] = neogridDatabaseConnectionIsAvailable()
            % Only for internal use at Neogrid (Tests whether a connection
            % to Neogrid's database is available)
            if exist('PreHEAT_internal.Configuration','class')==8 && ...
                    exist('PreHEAT_internal.DatabaseConnection','class')==8 && ...
                    exist('PreHEAT_internal.Dataset','class')==8
                
                try
                    connection_available = PreHEAT_internal.Configuration.databaseAccessIsAvailable();
                catch e
                    fprintf('PreHEAT_API.Data: [%s] %s\n', e.identifier, e.message);
                    connection_available = 0;
                end
                
            else
                connection_available = 0;
            end
        end
        
        function [data_table] = getDataFromNeogridDB(load_struct)
            
            options2use = struct(...
                'startDate',           load_struct.start_date , ...
                'endDate',             load_struct.end_date , ...
                'time_resolution',      load_struct.time_resolution ,  ...
                'interface',            'database' );
            
            switch load_struct.type
                
                case 'box'
                    data_table = PreHEAT_internal.Dataset.loadBuildingComponentData(...
                        load_struct.component_ids, options2use);
                    
                case 'weather'
                    data_table = PreHEAT_internal.Dataset.loadWeatherComponentData(...
                        load_struct.location_id,load_struct.weather_type_ids, options2use);
                    
                otherwise
                    data_table = [];
            end
        end
        
        function [] = checkValidResolution(time_resolution,data_type)
            
            % Check that the resolution is valid
            if PreHEAT_API.ApiConfig.onlyFastOperationAllowed()
                % Skip check when operating in fast operation mode
                return;
                
            elseif strcmp(data_type,'weather')
                accepted_resolutions = {'hour','day','week','month','year', '5min', 'raw'};
                if ~ischar(time_resolution) || ~ismember(time_resolution,accepted_resolutions)
                    error('Illegal time resolution. It must be a string with one of the following values: hour, day, week, month, year.');
                end
                
            elseif strcmp(data_type,'building')
                % Check that the resolution is valid
                accepted_resolutions = {'raw','minute','hour','day','week','month','year', '5min'};
                if ~ischar(time_resolution) || ~ismember(time_resolution,accepted_resolutions)
                    error('Illegal time resolution. It must be a string with one of the following values: raw, minute, hour, day, week, month, year.');
                end
                
            else
                error('Illegal data_type, it must be one of the following: weather, building.');
            end
        end
        
        function [data_timetable] = convertApiOutputToTimetable(apiOutputStruct, component_table, data_timetable )
            % Method loading sensor data from a start to an end date with a chosen time resolution
            %
            % Input:
            %       apiOutputStruct     Structure returned by the API call (struct)
            %       componentDictionary Correspondance between component ids and column names
            %                               (Matlab containers.map, with keys=component/type id and values=column name)
            %       data_timetable      Basic structure of the timetable (timetable, with columns t for time, and typically startDate and endDate)
            %
            % Output:
            %       data_timetable  Output of the API converted into a timetable of appropriate structure (Matlab timetable)
            %
            error('This function is DEPRECATED and will be REMOVED in a future version (make sure to load data as CSV rather than JSON)).');
        end
    end
end
