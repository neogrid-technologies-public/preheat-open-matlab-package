classdef Component < handle & matlab.mixin.Copyable
    %unit Defines a unit in the PreHEAT sense
    
    properties (GetAccess='public', SetAccess='protected')
        name            = '';   % Name of the component (string)
        data                    % Data for the component (PreHEAT_API.Data)
        unit            = '';
        stdUnitDivisor  = 1;    % Factor to divide by to obtain a standard unit
        stdUnit         = '?';  % Standard unit for this type of component
    end
    
    properties (GetAccess='protected', SetAccess='protected')
        id          = []    % Identifier of the component in unit (integer)
        cid         = []    % Identifier of the component (integer)
        type        = 'value'; 
        locationId  = NaN;        
    end
   
    properties (GetAccess='private', SetAccess='private')
        stateValue          = NaN;
        stateLatestUpdate   = NaT('TimeZone','UTC'); 
    end
    
    methods (Access='public')
        function obj = Component(input_data, location_id, component_type)            
            % Constructor creating a Component based upon a component id or
            % a structure.
            %
            % Inputs:
            %       input_data  Description of the component, either
            %                       component id (integer)
            %                           or
            %                       structure ( struct(cid,name) )
            %
            
            if nargin>=2
                obj.locationId = location_id;
            end
            if nargin>=3
                obj.type = component_type;
            end
            
            obj.data = PreHEAT_API.Data();
            
            if ~isempty(input_data) 
                
                % Affecting the id
                if isstruct(input_data)                    
                    if isfield(input_data,'cid')
                        obj.cid  = input_data.cid;
                    end
                    if isfield(input_data,'id')
                        obj.id  = input_data.id;
                    end
                    
                elseif isnumeric(input_data)
                    error('This use case is no longer valid');
                    obj.id = input_data;            
                else
                    error('Type of input is invalid');
                end
                
                % Affecting the name
                if isstruct(input_data) && isfield(input_data,'name')
                    obj.name  = input_data.name;                  
                elseif isfield(input_data,'id')
                    obj.name  = sprintf('id_%i',obj.id);
                elseif isfield(input_data,'cid')
                    obj.name  = sprintf('cid_%i',obj.cid);
                else
                    obj.name  = '?';
                end
            end
                        
            if isstruct(input_data)
                
                if isfield(input_data,'unit') && ~isempty(input_data.unit)
                    obj.unit = input_data.unit;
                end
                
                if isfield(input_data,'stdUnitDivisor') && ~isempty(input_data.stdUnitDivisor)
                    obj.stdUnitDivisor = input_data.stdUnitDivisor;
                end
                
                if isfield(input_data,'stdUnit') && ~isempty(input_data.stdUnit)
                    obj.stdUnit = input_data.stdUnit;
                end
                                
            end
            
            % Check that the name is valid
            obj.name = PreHEAT_API.Component.validateName(obj.name);
            
        end
        
        function [id,cid] = getId(obj)
            % Method to access the id of a component.
            %
            % Inputs:
            %       /
            %
            % Outputs:
            %       id   	The unit id of the component
            %       cid     The component id of the component
            %
            
            if isempty(obj.id)
                id  = NaN;
            else
                id  = obj.id;
            end            
            if isempty(obj.cid)
                cid  = NaN;
            else
                cid = obj.cid;
            end
        end
        
        function loadData(obj, start_date, end_date, time_resolution)
            % Method to load data for the component over a given period.
            %
            % Inputs:
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of the current object
            %
                               
            component_details = obj.getDetails();
            
            % Adapting the data to match the format for a query at Unit level
            component_details.locationId= {component_details.locationId};
            component_details.name     	= {component_details.name};
            component_details.stdUnit	= {component_details.stdUnit};
            component_details.unit      = {component_details.unit};
            component_details.type      = {component_details.type};
            
            % Loads the building data            
            obj.data.refreshComponentNames(component_details,'box_data');
            obj.data.loadData( start_date,end_date,time_resolution );
        end
        
        function importData(obj, data_table, start_date, end_date, time_resolution)
            % Method to import data from a large data table.
            %
            % Inputs:
            %       data_table      Table containing the measurement data (columns: id,epochtime,value)
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of the current object
            %
                                     
            if isempty(obj.id)
                return;
            end
            
            all_cids = obj.getDetails();
            
            % Adapting the data to match the format for a query at Unit level
            all_cids.name     	= {all_cids.name};
            all_cids.stdUnit	= {all_cids.stdUnit};
            all_cids.unit       = {all_cids.unit};
            all_cids.type       = {all_cids.type};
            
            obj.data.importData(data_table, all_cids, start_date, end_date, time_resolution);                        
        end
        
        function clearData(obj)
            % Method to load data for all components of the unit over a given period.
            %
            % Inputs:
            %       /
            %
            % Outputs:
            %       /  
            %
                      
            % Clears the data
            obj.data.clearData();           
        end
        
        function mergeData(obj, component_with_extra_data)
            obj.data.mergeData(component_with_extra_data.data);
        end
        
        function removeData(obj, criterion, detail)
            obj.data.removeData(criterion, detail);
        end
        
        function component_details = getDetails(obj)
            % Method to get the content of the object in a struct format
            %
            % Inputs:
            %       /       
            %
            % Outputs:
            %       componentDetails        Structure containing the details of the current object (struct(name,stdUnitDivisor,stdUnit))
            %
            component_details        = table;
            
            if ~isempty(obj.id)
                component_details.id	= obj.id;
            else
                component_details.id	= NaN;
            end
            if ~isempty(obj.cid)
                component_details.cid	= obj.cid;
            else
                component_details.cid	= NaN;
            end
            
            if ~isempty(obj.name)
                component_details.name = obj.name;
            else
                component_details.name = ' '; % Space, as empty string is not accepted in table
            end
            
            if ~isempty(obj.stdUnitDivisor)
                component_details.stdUnitDivisor= obj.stdUnitDivisor;                
            else
                component_details.stdUnitDivisor= 1;
            end
            
            if ~isempty(obj.stdUnit)
                component_details.stdUnit	= obj.stdUnit;
            else
                component_details.stdUnit	= ' '; % Space, as empty string is not accepted in table
            end
            
            if ~isempty(obj.unit)
                component_details.unit      = obj.unit;
            else
                component_details.unit      = ' '; % Space, as empty string is not accepted in table
            end
            
            if ~isempty(obj.type)
                component_details.type      = obj.type;
            else
                component_details.type      = ' '; % Space, as empty string is not accepted in table
            end
            
            if ~isempty(obj.locationId)
                component_details.locationId      = obj.locationId;
            else
                component_details.locationId      = NaN;
            end
        end

        function [latest_value] = getCurrentValue(obj, delay_tolerance, present_time)
                        
            if nargin<2
                delay_tolerance = minutes(5);
            end            
            if nargin<3
                present_time = datetime('now','TimeZone','Europe/Copenhagen');
            end
            
            obj.updateState('raw', present_time, delay_tolerance);
            [latest_value, ~] = obj.getState();
        end
        
        function [empty_component] = isempty(obj)
            empty_component = (length(obj)==0) || isempty(obj.id) || isnan(obj.id);
        end
        
        function setType(obj,new_type)
            if isempty(new_type)
                error("Empty type not allowed");
            elseif ~ischar(new_type)
                error("Type must be a char");
            else
                obj.type = new_type;
            end
        end
        
        % Functions to manage state
        function [value, last_update] = getState(obj)
            value = obj.stateValue;
            
            if nargout>=2
                last_update = obj.stateLatestUpdate;
            end
        end
        function setState(obj, value, last_update)
            if ~isnumeric(value)
               error('Input "value" must be a number.'); 
            end
            if ~isdatetime(last_update)
               error('Input "last_update" must be a datetime.'); 
            end
            obj.stateValue        = value;
            obj.stateLatestUpdate = last_update;
        end
        function updateState(obj, varargin)
            %updateState Updates the state of a component
            %
            % Valid calls:
            %   updateState(obj)
            %   updateState(obj, time_resolution)
            %   updateState(obj, time_resolution, update_time)
            %   updateState(obj, time_resolution, update_time, expiry_limit)
            %   updateState(obj, 'average', averaging_period)
            %   updateState(obj, 'average', averaging_period, update_time)
            %
            % Precisions:
            % - 'average' mode works with raw data
            % - the defaut value for "update_time" is the current time
            %
            if (isempty(obj.id) || isnan(obj.id)) && (isempty(obj.cid) || isnan(obj.cid))
                return;
            end
            
            if nargin>=2 && strcmp(varargin{1},'average')
                import_type = 'average';
                averaging_period = varargin{2};                                
                if nargin<4
                    update_time = datetime('now','TimeZone','UTC');
                else
                    update_time = varargin{3};
                end
                data_table = PreHEAT_API.Data.fetchComponentData(obj.id, ...
                    update_time-averaging_period,update_time,'raw','id');
                
            else
                import_type = 'latest';
                
                if nargin==1
                    data_table  = PreHEAT_API.Data.fetchComponentData(obj.id, 'latest_id');
                elseif nargin==2
                    time_resolution = varargin{1};
                    data_table  = PreHEAT_API.Data.fetchComponentData(obj.id, 'latest_id', time_resolution);
                elseif nargin>=3
                    time_resolution = varargin{1};
                    update_time = varargin{2};
                    if nargin>=4
                        expiry_limit = varargin{3};
                    else
                        expiry_limit = hours(1);
                    end
                    start_date = update_time - expiry_limit;
                    data_table = PreHEAT_API.Data.fetchComponentData(obj.id, start_date, update_time, time_resolution, 'latest_id');
                    
                else
                    error('Not supposed to be here.');
                end
            end
            
            obj.importState(data_table,import_type);
        end
        function importState(obj, data_table, import_type)
            
            if nargin<3
                import_type = 'latest';
            end
            
            if isempty(data_table) || isempty(obj.id)
                obj.setState(NaN, NaT('TimeZone','UTC'));
            else
                focus_data  = data_table(data_table.id==obj.id,:);
                
                if isempty(focus_data)
                    obj.setState(NaN, NaT('TimeZone','UTC'));
                else                    
                    [~,k_latest] = max(focus_data.epochtime);
                    last_update = datetime( focus_data.epochtime(k_latest), 'ConvertFrom', 'epochtime', 'TimeZone', 'UTC' );
                    
                    switch import_type
                        case 'latest'
                            value = focus_data.normalised_value(k_latest);
                            
                        case 'average'
                            value = mean(focus_data.normalised_value,'omitnan');
                            
                        otherwise
                            error('Illegal "import_type" (%s)');
                    end
                    
                    obj.setState(value, last_update);
                end
            end
        end
    end
    
    methods (Access='protected')
        function [copy_of_obj] = copyElement(obj)
           copy_of_obj = copyElement@matlab.mixin.Copyable(obj);
           
           % Do not copy the data
           copy_of_obj.data = PreHEAT_API.Data();
        end
    end
        
    methods (Static, Access='public')
        
        function [validated_name] = validateName(original_name)  
            % Method to validate the name of a component.
            %
            % Inputs:
            %       original_name   Initial name (string)
            %
            % Outputs:
            %       validated_name  Initial name, corrected for illegal
            %                           characters (string)
            %
            
            % Checking that the name does not contain space (or
            % any unauthorised characters
            validated_name = strrep(original_name,' ','_');            
        end
    end
end

