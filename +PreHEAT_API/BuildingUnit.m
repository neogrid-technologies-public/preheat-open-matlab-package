classdef BuildingUnit < PreHEAT_API.Unit
    % Defines a building unit in the PreHEAT sense.
    %
    % This class is a handle class and extends PreHEAT_API.Unit.
    
    properties (GetAccess='public', SetAccess='protected')
        % Inherited from PreHEAT_API.Unit
        % +
        subUnits    = []; % Sub-units (PreHEAT_API.BuildingUnit array)
        controlUnits= []; % Control units (PreHEAT_API.ControlUnit array)
        meters      = []; % Meters linked to the unit
        
        parentUnit      = []; % Reference to the parent unit (PreHEAT_API.BuildingUnit)
        parentBuilding  = []; % Reference to the parent building (PreHEAT_API.BuildingUnit)
    end
    
    properties (GetAccess='protected',SetAccess='protected')
        produces    = ''; % For heat-pumps : 'HEAT', 'COOLING', 'HEAT_AND_COOLING'
        coversWholeBuilding = [];
        coveredZones = []; % Zones covered (PreHEAT_API.Zone array)
        
        placedBeforeHeatRecovery = NaN;
        placementNumber = NaN;
        coils = [];
    end
    
    methods (Access='public')
        function obj = BuildingUnit(struct_to_create_unit, parent_building, unit_type, parent_unit,fields2ignore)
            % Constructor creating a Data object out of a structure with following fields
            %
            % Inputs:
            %       struct_unit         Structure containing the relevant fields
            %                       for the desired object create among:
            %                           id                  Id of the unit (integer)
            %                           name                Name of the unit(string)
            %                           components          Array of component structures (array of struct(name,cid) )
            %                           zoneIds / zoneId    containers.Map (keys: type ids of the weather data & values: signal names) or [])
            %                           secondaries         Secondaries of the unit (array of PreHEAT_API.Unit)
            %                           coversBuilding      Whether the unit covers the whole building (true) or just a specific zone (false) (boolean)
            %                           type                (unused yet)
            %                           controls            (unused yet)
            %
            %       parent_building     Handle to parent building
            %       
            %       unit_type           Type of the unit (string)
            %
            %       parent_unit         Parent unit (PreHEAT_API.BuildingUnit)
            
            if nargin<2
                parent_building = [];
            end
            
            if nargin<3
                unit_type = '';
            end
            
            if nargin<4
                parent_unit = [];
            end
            if nargin<5
                fields2ignore = {};
            end
            
            % Log the location id, if the building is provided
            if ~isempty(parent_building) && ...
                    isstruct(parent_building.location) && isfield(parent_building.location,'locationId')
                location_id = parent_building.location.locationId;
            end
            
            % Initialise with parent class constructor
            
            % (Mark the fields below to be ignored)
            unit_fields2ignore = union(fields2ignore,...
                {'coversBuilding','controls','zoneIds','zoneId',...
                'coldWaterId','electricityId',...
                'mainId','heatingId','heatingSecondaryId','hotWaterId','heatPumpIds','subMeters',...
                'coolingId','coolingSecondaryId','heatingCoilIds','coolingCoilIds',...
                'subHeating','secondaryTank','secondaryCirculation','secondaryHeatExchanger',...
                'secondaries','subHeating','heatingCoils','coolingCoils','heatRecovery','subVentilations'...                
                'placedBeforeHeatRecovery','placementNumber',...
                'shared','buildingsCovered','sharedFrom','sharedLocations','produces'});
                                    
            obj@PreHEAT_API.Unit(struct_to_create_unit,unit_type,location_id,unit_fields2ignore);
            
            
            % If the structure is empty, create an empty unit
            if nargin==0 || isempty(struct_to_create_unit)
                return
            elseif length(struct_to_create_unit)>1
                error('There can be maximum one element per Unit in the call to the constructor.');
            end
                        
            
            % Link the parent unit, if provided
            if ~isempty(parent_unit)
            	obj.parentUnit = parent_unit;
            end
            % Link the parent building, if provided
            if ~isempty(parent_building)
                obj.parentBuilding = parent_building;
            end
            coils2use = PreHEAT_API.BuildingUnit.empty;
            
            % Structure the fields in the appropriate components
            F = setdiff(fieldnames(struct_to_create_unit),fields2ignore);
            
            for i=1:length(F)
                F_i = F{i};
                
                if strcmp(F_i,'id') || strcmp(F_i,'name') || strcmp(F_i,'type') || isfield(obj.components,F_i) || ...
                        isfield(obj.ignoredElements,F_i)
                    % Has been handled by the parent constructor already
                else
                    
                    switch F_i
                        case {'zoneIds','zoneId'}
                            zones2add = struct_to_create_unit.(F_i);
                            for j=1:length(zones2add)
                                zone_j = parent_building.getZone(zones2add(j));
                                obj.addZone(zone_j);                                
                            end
                            
                        case {'coversBuilding'}
                            obj.coversWholeBuilding = struct_to_create_unit.coversBuilding;
                            
                        case 'controls'
                            controls2add = struct_to_create_unit.(F_i);
                            
                            for j=1:length(controls2add)
                                control_j = controls2add(j);
                                % Only adding control units if they are not empty
                                if ~isempty(control_j)
                                    control_unit2add = PreHEAT_API.ControlUnit(struct_to_create_unit.(F_i)(j), parent_building, obj);
                                    
                                    obj.addControlUnit( control_unit2add );
                                end
                            end
                            
                        case 'subMeters'
                            meters2add = struct_to_create_unit.(F_i);
                            
                            for j=1:length(meters2add)
                                
                                meter_j = meters2add(j);
                                
                                switch obj.type
                                    case {'heating','secondaries'}
                                        submeter_type_ij = 'heating_subMeter';
                                        
                                    case {'hotWater','secondaryTank','secondaryHeatExchanger'}
                                        submeter_type_ij = 'hotWater_subMeter';
                                        
                                    case 'electricity'
                                        submeter_type_ij = 'electricity_subMeter';
                                        
                                    otherwise
                                        warning('No specific subMeters type handling for unit "%s".',obj.type);
                                        submeter_type_ij = F_i;
                                end
                                
                                meter_unit_ij = PreHEAT_API.BuildingUnit(meter_j, parent_building,submeter_type_ij);
                                
                                obj.addSubUnit( meter_unit_ij, submeter_type_ij );
                            end
                            
                        case {'coldWaterId','electricityId','mainId','heatPumpIds'}
                            meters2add = struct_to_create_unit.(F_i);
                            meter_type = strrep(F_i,'Id','');
                            
                            for j=1:length(meters2add)
                                
                                meter_j = meters2add(j);
                                
                                if isempty(obj.meters)
                                    obj.meters = struct;
                                end
                                if ~isfield(obj.meters,meter_type)
                                    obj.meters.(meter_type) = [];
                                end
                                
                                switch meter_type
                                    case 'electricity'
                                        meter_unit_ij = parent_building.getElectricityMeter(meter_j);
                                    case 'main'
                                        meter_unit_ij = parent_building.getMainMeter(meter_j);
                                    case 'coldWater'
                                        meter_unit_ij = parent_building.getWaterMeter(meter_j);
                                    case 'heatPumps'
                                        meter_unit_ij = parent_building.getHeatPump(meter_j);                                        
                                    otherwise
                                        meter_unit_ij = [];
                                end
                                
                                obj.addMeter( meter_unit_ij, meter_type );
                            end
                            
                        case {'heatingId','hotWaterId','coolingId','heatingSecondaryId','coolingSecondaryId'}
                            loops2add = struct_to_create_unit.(F_i);
                            loop_type = strrep(F_i,'Id','');
                            
                            for j=1:length(loops2add)
                                
                                loop_j = loops2add(j);
                                
                                switch loop_type
                                    case 'heating'
                                        loop_unit_ij = parent_building.getSpaceHeatingUnit(loop_j);
                                    case 'heatingSecondary'
                                        loop_unit_ij = parent_building.getSpaceHeatingSecondaryUnit(loop_j);
                                    case 'cooling'
                                        loop_unit_ij = parent_building.getCoolingUnit(loop_j);                                        
                                    case 'coolingSecondary'
                                        loop_unit_ij = parent_building.getSpaceCoolingSecondaryUnit(loop_j);
                                    case 'hotWater'
                                        loop_unit_ij = parent_building.getHotWaterUnit(loop_j);
                                    otherwise
                                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                                            sprintf('Need to handle subUnit type: %s',loop_type),...
                                            'BuildingUnit' );
                                        loop_unit_ij = [];
                                end
                                
                                obj.addSubUnit( loop_unit_ij, loop_type );
                            end
                            
                        case {'secondaries','subHeating',...
                                'heatingCoils','coolingCoils',...
                                'subVentilations','heatRecovery',...
                                'secondaryCirculation','secondaryTank','secondaryHeatExchanger'...
                                }
                            for j=1:length(struct_to_create_unit.(F_i))
                                % Only adding units if they are not empty
                                if ~isempty(struct_to_create_unit.(F_i)(j))
                                    obj.addSubUnit( ...
                                        PreHEAT_API.BuildingUnit(struct_to_create_unit.(F_i)(j), parent_building, F_i, obj), F_i );
                                end
                            end
                                                        
                        case 'produces'
                            obj.produces = struct_to_create_unit.(F_i);
                            
                        case {'shared','buildingsCovered','sharedFrom','sharedLocations'}
                            % Units shared between buildings are not supported yet.
                            
                        case 'placedBeforeHeatRecovery'
                            obj.placedBeforeHeatRecovery = struct_to_create_unit.placedBeforeHeatRecovery;
                        case 'placementNumber'
                            obj.placementNumber = struct_to_create_unit.placementNumber;
                            
                        case {'heatingCoilIds','coolingCoilIds'}
                            
                            if strcmp(F_i,'heatingCoilIds')
                                coil_type = 'heating';
                            else
                                coil_type = 'cooling';                                
                            end
                            
                            coil_ids = struct_to_create_unit.(F_i);
                            
                            for k_coil=1:length(coil_ids)
                                coils2use(end+1) = parent_building.getBuildingUnit( coil_ids(k_coil), coil_type );
                            end
                                        
                        otherwise
                            PreHEAT_API.ApiConfig.flagUpdateRequired(...
                                sprintf('Field "%s" must be handled automatically.',F_i),...
                                'BuildingUnit' );
                            obj.logIgnoredElement(F_i);
                    end
                end
            end
            
            if ~isempty(coils2use)
                obj.coils = coils2use;
            end
            
            obj.orderElementsById();
        end
        
        function all_cids = getAllComponentIds(obj,add_prefix, load_subcomponents)
            % Method to get all ids from the components of the unit.
            %
            % Inputs:
            %       add_prefix  Choose to add (1) or ignore (0) the unit name in the component name (boolean)
            %       add_postfix Choose to add (1) or ignore (0) some extra termination (boolean)
            %
            % Outputs:
            %       all_cids    Dictionary of all component ids of the unit (containers.Map with key=component id and value=component name)
            %
            if nargin<2
                add_prefix = false;
            end
            if nargin<3
                load_subcomponents = true;
            end
            
            % Now disabling the prefix addition (until another solution is
            % found)
            add_prefix = false;
            add_prefix_subunits  = add_prefix;
            
            % Call the method for details and only keep the relevant part
            no_prefix_subunit = '';
            all_cids = obj.getAllComponentDetails(add_prefix,load_subcomponents,no_prefix_subunit);            
            all_cids = all_cids(:,{'id','name','extendedName'});
        end
        
        function componentDetails = getAllComponentDetails(obj,add_prefix,load_subcomponents,add_prefix_subunits)
            % Method to get all ids from the components of the unit.
            %
            % Inputs:
            %       add_prefix  Choose to add (1) or ignore (0) the unit name in the component name (boolean)
            %
            % Outputs:
            %       all_cids    Dictionary of all component ids of the unit (containers.Map with key=component id and value=component name)
            %
            if nargin<2
                add_prefix = false;
            end
            if nargin<3
                load_subcomponents = true;
            end
            if nargin<4                
                add_prefix_subunits  = 'sub_';
            end
            
            % Call the method of the Unit class
            componentDetails = getAllComponentDetails@PreHEAT_API.Unit(obj,add_prefix);
            
            % Extend with the loading of all the cids of the subUnits and controlUnits
            if load_subcomponents
                
                for i=1:length(obj.subUnits)
                    all_details_i	 = obj.subUnits(i).getAllComponentDetails(add_prefix_subunits,load_subcomponents);
                    
                    if ~isempty(all_details_i)
                        all_details_i.extendedName  = strcat(obj.type,'.',all_details_i.extendedName);
                        componentDetails = [ componentDetails ; all_details_i ];
                    end
                end
                
                for i=1:length(obj.controlUnits)
                    all_details_i	 = obj.controlUnits(i).getAllComponentDetails(add_prefix_subunits);
                    
                    if ~isempty(all_details_i)
                        all_details_i.extendedName  = strcat(obj.type,'.',all_details_i.extendedName);
                        componentDetails = [ componentDetails ; all_details_i ];
                    end
                end
                                
                if ~isempty(obj.meters)
                    all_meters = fieldnames(obj.meters);
                    
                    for i=1:length(all_meters)
                        for j=1:length(obj.meters.(all_meters{i}))
                            all_cids_i_j	= obj.meters.(all_meters{i})(j).getAllComponentDetails(add_prefix_subunits);
                            if ~isempty(all_cids_i_j)
                                all_cids_i_j.extendedName  = strcat(obj.type,'.',all_cids_i_j.extendedName);
                                componentDetails    = [ componentDetails ; all_cids_i_j ];
                            end
                        end
                    end
                end
            end
            
        end
        
        function [connectedUnitsDetails] = getAllConnectedUnitsDetails(obj, prefix_connection)
            
            if isempty(obj)
               connectedUnitsDetails = [];
               return;
            end
            
            if nargin<2
                prefix_connection = '';
            elseif ~isempty(prefix_connection) && ~endsWith(prefix_connection,'.')
                prefix_connection = [prefix_connection,'.'];
            end
            
            if isempty(prefix_connection)
                prefix_connection_without_dot = '';
            else
                prefix_connection_without_dot = prefix_connection(1:end-1);
            end
                        
            % Add details of unit
            connectedUnitsDetails = obj.getUnitDetails(prefix_connection_without_dot);
            
            % Add details of subUnits, meters and control units
            for i=1:length(obj.subUnits)
                connectedUnitsDetails = [ connectedUnitsDetails ; ...
                    obj.subUnits(i).getAllConnectedUnitsDetails([prefix_connection,'subUnit']) ];
            end
            for i=1:length(obj.controlUnits)
                connectedUnitsDetails = [ connectedUnitsDetails ; ...
                    obj.controlUnits(i).getAllConnectedUnitsDetails([prefix_connection,'controlUnit']) ];
            end
            
            if isempty(obj.meters)
                meter_fields = {};
            else
                meter_fields = fieldnames(obj.meters);                
            end            
            for i=1:length(meter_fields)
                meter_i = meter_fields{i};
                connectedUnitsDetails = [ connectedUnitsDetails ; ...
                    obj.meters.(meter_i).getAllConnectedUnitsDetails([prefix_connection,'meter.',meter_i]) ];
            end
        end
        
        function loadData(obj,start_date, end_date, time_resolution, options2use,add_prefix)
            % Method to load data for all components of the building unit over a given period.
            %
            % Inputs:
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of the current object
            %
            
            if nargin<5
                options2use = struct();
            elseif nargin>=5 && isnumeric(options2use)
                % For backwards compatibility
                load_subcomponents_in_unit = options2use;
                 options2use = struct('loadSubcomponentsInUnit',load_subcomponents_in_unit);
            elseif ~isstruct(options2use)
                error('Input "options2use" must be a struct');
            end
            
            if nargin>=6
                % For backwards compatibility
                options2use.addPrefix = add_prefix;
            end            
            options2use = PreHEAT_API.BuildingUnit.autocompleteOptionsLoading(options2use);
            
            component_details = obj.selectFocusComponents( ...
                options2use.limitComponents, options2use.addPrefix, options2use.loadSubcomponentsInUnit  );
            
            % Loads the building data
            obj.data.refreshComponentNames(component_details,'box_data');
            obj.data.loadData(start_date,end_date,time_resolution );
            
            if ~options2use.loadSubcomponentsInUnit && options2use.allowSubUnitsLoading
                
                for i=1:length(obj.subUnits)
                    options2use_i = obj.limitComponentsSubunit( ...
                        options2use , obj.subUnits(i) ); 
                    obj.subUnits(i).loadData(start_date, end_date, time_resolution, options2use_i);
                end
                
                for i=1:length(obj.controlUnits)
                    options2use_i = obj.limitComponentsSubunit( ...
                        options2use , obj.controlUnits(i) ); 
                    obj.controlUnits(i).loadData(start_date, end_date, time_resolution, options2use_i);
                end
            end            
        end
        
        function importData(obj, data_table, start_date, end_date, time_resolution, options2use)
            % Method to load data for all components of the unit over a given period.
            %
            % Inputs:
            %       /
            %
            % Outputs:
            %       /
            %
            if nargin<6
                options2use = struct();
            elseif nargin>=6 && isnumeric(options2use)
                % For backwards compatibility
                load_subcomponents_in_unit = options2use;
                 options2use = struct('loadSubcomponentsInUnit',load_subcomponents_in_unit);
            elseif ~isstruct(options2use)
                error('Input "options2use" must be a struct');
            end    
            options2use.addPrefix = 0;
            options2use = PreHEAT_API.BuildingUnit.autocompleteOptionsLoading(options2use);
                                                
            % Calling method in parent class
            component_table = obj.getAllComponentDetails(options2use.addPrefix,options2use.loadSubcomponentsInUnit);
            obj.data.importData(data_table, component_table, start_date, end_date, time_resolution, 'box_data');
            
            % Calling on sub-units and controllers if needed
            if ~options2use.loadSubcomponentsInUnit
                
                for i=1:length(obj.subUnits)
                    options2use_i = obj.limitComponentsSubunit( ...
                        options2use , obj.subUnits(i) ); 
                    obj.subUnits(i).importData(data_table, start_date, end_date, time_resolution, options2use_i);
                end
                
                for i=1:length(obj.controlUnits)
                    options2use_i = obj.limitComponentsSubunit( ...
                        options2use , obj.controlUnits(i) ); 
                    obj.controlUnits(i).importData(data_table, start_date, end_date, time_resolution, options2use_i);
                end
            end
        end
        
        function clearData(obj)
            % Method to load data for all components of the unit over a given period.
            %
            % Inputs:
            %       /
            %
            % Outputs:
            %       /
            %
            
            % Clears the data
            clearData@PreHEAT_API.Unit(obj);
            % Clearing the data on all sub-units
            for i=1:length(obj.subUnits)
                obj.subUnits(i).clearData();
            end
            % Clearing the data on all control units
            for i=1:length(obj.controlUnits)
                obj.controlUnits(i).clearData();
            end            
        end
                
        function mergeData(obj, unit_with_extra_data)
            
            mergeData@PreHEAT_API.Unit(obj,unit_with_extra_data);
            
            if length(obj.subUnits)~=length(unit_with_extra_data.subUnits)
                error('Units have differing subUnits structures.')
            end
            if length(obj.controlUnits)~=length(unit_with_extra_data.controlUnits)
                error('Units have differing controlUnits structures.')
            end
            
            % Clearing the data on all sub-units
            for i=1:length(obj.subUnits)
                obj.subUnits(i).mergeData(unit_with_extra_data.subUnits(i));
            end
            % Clearing the data on all control units
            for i=1:length(obj.controlUnits)
                obj.controlUnits(i).mergeData(unit_with_extra_data.controlUnits(i));
            end            
        end
        
        function removeData(obj, criterion, detail)
            
            removeData@PreHEAT_API.Unit(obj, criterion, detail);
                        
            % Clearing the data on all sub-units
            for i=1:length(obj.subUnits)
                obj.subUnits(i).removeData(criterion, detail);
            end
            % Clearing the data on all control units
            for i=1:length(obj.controlUnits)
                obj.controlUnits(i).removeData(criterion, detail);
            end            
        end
        
        function [data] = getData(obj, get_sub_unit_data)
            if nargin<2
                get_sub_unit_data = true;
            end
            
            if isempty(obj)
                data = PreHEAT_API.Data;
                return
            end
            
            if isempty(obj.data)
                data = PreHEAT_API.Data;
            else
                data = copy(obj.data);
            end
            
            if get_sub_unit_data
                for i=1:length(obj.subUnits)
                    data_i = obj.subUnits(i).getData();
                    if ~isempty(data_i)
                        data.addData(data_i, data_i.resolution);
                    end
                end
                
                for i=1:length(obj.controlUnits)
                    data_i = obj.controlUnits(i).getData();
                    if ~isempty(data_i)
                        data.addData(data_i, data_i.resolution);
                    end
                end
            end
            
        end
        
        function [subunits_of_given_type] = getSubUnit(obj,target_type)
            
            subunits_of_given_type = PreHEAT_API.BuildingUnit.empty;
            
            N_subunits = length(obj.subUnits);
            N_controls = length(obj.controlUnits);
            N_meters   = length(obj.meters);
            
            for i=1:(N_subunits+N_controls+N_meters)
               
                if i<=N_subunits
                    if strcmp(obj.subUnits(i).type,target_type)
                        subunits_of_given_type(end+1) = obj.subUnits(i);
                    end
                    
                elseif i<=N_subunits+N_controls
                    i2use = i-N_subunits;
                    if strcmp(obj.controlUnits(i2use).type,target_type)
                        subunits_of_given_type(end+1) = obj.controlUnits(i2use);
                    end
                    
                else
                    i2use = i-N_subunits-N_controls;
                    if isfield(obj.meters(i2use),target_type)
                        
                        meters2use = obj.meters(i2use).(target_type);
                        for j=1:length(meters2use)
                            subunits_of_given_type(end+1) = meters2use(j);
                        end
                    end
                end
            end
        end
        
        function addControlUnit(obj,controlUnits_2_add)
            
            if isempty(controlUnits_2_add)
                return;
            end
            
            if isempty(obj.controlUnits)
                obj.controlUnits = PreHEAT_API.ControlUnit.empty;
            end
            obj.controlUnits(end+1) = controlUnits_2_add;
        end
        
        function [produces_cooling] = canProduceCooling(obj)
            
            if ~isempty(obj.produces)
                switch obj.produces
                    case 'HEAT'
                        produces_cooling = false;
                        
                    case {'COOLING','HEAT_AND_COOLING'}
                        produces_cooling = true;
                        
                    otherwise
                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                            sprintf('Unknown value of parameter "produces" (%s)',obj.produces),...
                            'BuildingUnit' );
                        produces_cooling = NaN;
                end
            else
                switch obj.type
                    case 'cooling'
                        produces_cooling = true;
                        
                    case 'heatPumps'
                        warning("The status of the heat pump (reversible/chiller/heat-pump) has not been filled (locationId=%i).",obj.locationId);
                        produces_cooling = NaN;
                        
                    case {'main','heating','hotWater'}
                        produces_cooling = false;
                        
                    case {'indoorClimate','coldWater','electricity','custom','ventilation'}
                        produces_cooling = false;
                        
                    otherwise
                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                            sprintf('Unknown value of parameter "produces" (%s)',obj.produces),...
                            'BuildingUnit' );
                        produces_cooling = NaN;
                end
            end
        end
        
        function [produces_heating] = canProduceHeating(obj)
            if ~isempty(obj.produces)
                switch obj.produces
                    case 'COOLING'
                        produces_heating = false;
                        
                    case {'HEAT','HEAT_AND_COOLING'}
                        produces_heating = true;
                        
                    otherwise
                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                            sprintf('Unknown value of parameter "produces" (%s)',obj.produces),...
                            'BuildingUnit' );
                        produces_heating = NaN;
                end
            else
                switch obj.type
                    case {'main','heating','hotWater'}
                        produces_heating = true;
                        
                    case 'heatPumps'
                        warning("The status of the heat pump (reversible/chiller/heat-pump) has not been filled (locationId=%i).",obj.locationId);
                        produces_heating = NaN;
                        
                    case 'cooling'
                        produces_heating = false;
                        
                    case {'indoorClimate','coldWater','electricity','custom','ventilation'}
                        produces_heating = false;
                        
                    otherwise
                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                            sprintf("Unknown value of type (%s)",obj.type),...
                            'BuildingUnit' );
                        produces_heating = NaN;
                end
            end
        end
        
        function [unit_covers_whole_building] = coversBuilding(obj)
            
            if isempty(obj.coversWholeBuilding)
                unit_covers_whole_building = false;
            else
                unit_covers_whole_building = obj.coversWholeBuilding;
            end
        end
        
        function addSubUnit(obj,subUnits_2_add, type_subunit)
            
            if isempty(subUnits_2_add)
                return;
            end
            
            if nargin<3
                error('The sub-unit type should be specified');
            elseif ~ischar(type_subunit)
                disp(type_subunit);
                error('Illegal "type_subunit" provided (must be a string).');
            end
            
            subUnits_2_add.type = type_subunit;
                        
            % Adding to subUnits
            if isempty(obj.subUnits)
                obj.subUnits = PreHEAT_API.BuildingUnit.empty;
            end
            obj.subUnits(end+1) = subUnits_2_add;            
        end
        function addZones(obj,zones2add)
            
            for i=1:length(zones2add)
                obj.addZone(zones2add(i));
            end
        end
        
        function [unit] = getUnitById(obj,unit_id)
            
            % Check if the unit is the desired one
            if unit_id==obj.id
                unit = obj;
                return;
            end
            
            % Check if the desired one is contained in sub-units
            for j=1:length(obj.controlUnits)
                unit = obj.controlUnits(j).getUnitById(unit_id);
                if ~isempty(unit)
                    return;
                end
            end
            for j=1:length(obj.subUnits)
                unit = obj.subUnits(j).getUnitById(unit_id);
                if ~isempty(unit)
                    return;
                end
            end
            
            % Else, return an empty unit
            unit = [];
        end
        
        function [zones2use] = getZones(obj)
            zones2use = obj.coveredZones;
        end
                
        function [unit_type, system_type] = getType(obj)
            unit_type = getType@PreHEAT_API.Unit(obj);
            
            if nargout>=2
                if ismember(unit_type,obj.systemTypeNames())
                    system_type = unit_type;
                else
                    if ~isempty(obj.parentUnit)
                        [~,system_type] = obj.parentUnit.getType();
                    else
                        system_type = '?';
                    end
                end
            end
        end
        
        function [coils, position, type, before_HR] = getCoils(obj, coil_type)
                        
            coils = obj.coils;
            N_coils = length(coils);
            position = NaN(N_coils,1);
            before_HR = NaN(N_coils,1);
            type = cell(N_coils,1);
            
            for i=1:N_coils
                coil_i = coils(i);
                if ~isempty(coil_i.placementNumber)
                    position(i) = coil_i.placementNumber;
                end
                type{i} = coil_i.getType();
                before_HR(i) = coil_i.placedBeforeHeatRecovery;
            end
            
            % Renaming the coils
            type = strrep(type,'heatingCoils','heating');
            type = strrep(type,'coolingCoils','cooling');
            
            if nargin>=2
                k_selected = find(strcmp(type,coil_type));
                coils = coils(k_selected);
                position = position(k_selected);
                type = type{k_selected};
                before_HR = before_HR(k_selected);
            end
        end
                
        function [unit_contains_component] = hasA(obj, component_type)
            
            switch component_type
                
                case 'heatRecovery'
                    unit_contains_component = 0;
                    for i=1:length(obj.subUnits)
                        unit_contains_component = unit_contains_component || ...
                            strcmp(obj.subUnits(i).getType(),'heatRecovery');
                    end
                    
                case {'heatingCoil','coolingCoil'}
                    relevant_coils = obj.getCoils(strrep(component_type,'Coil',''));
                    unit_contains_component = ~isempty(relevant_coils);
                    
                otherwise
                    unit_contains_component = hasA@PreHEAT_API.Unit(obj,component_type);
            end
        end
        
        function [control_units] = getControlUnits(obj, look_in_subunits)
            
            if nargin<2
                look_in_subunits = 0;
            end
            
            if isempty(obj.controlUnits)
                control_units = PreHEAT_API.ControlUnit.empty;
            else
                control_units = [obj.controlUnits]';
            end
            
            if look_in_subunits
                for i=1:length(obj.subUnits)
                   control_units = [control_units;obj.subUnits(i).getControlUnits(look_in_subunits)]; 
                end
            end
        end
        function [control_unit_ids] = getControlUnitIds(obj, look_in_subunits)
            
            if nargin<2
                look_in_subunits = 0;
            end
            
            if isempty(obj.controlUnits)
                control_unit_ids = [];
            else
                control_unit_ids = [obj.controlUnits.id]';
            end
            
            if look_in_subunits
                for i=1:length(obj.subUnits)
                   control_unit_ids = [control_unit_ids;obj.subUnits(i).getControlUnitIds(look_in_subunits)]; 
                end
            end
        end
        
        % For backwards compatibility
        function [zones2use] = zones(obj,i)
            if nargin==1
                zones2use = obj.getZones();
            else
                zones2use = obj.coveredZones(i);
            end
        end
    end
    
    methods (Access='protected')
                
        function addMeter(obj,meter_2_add,meter_type)
            
            if isempty(meter_2_add)
                return;
            end
            
            if nargin<3
                error('The meter type should be specified');
                
            elseif ~ischar(meter_type) || ~ismember(meter_type,{'main','coldWater','electricity','heatPumps'})
                disp(meter_type);
                error('Illegal "meter_type" provided.');
            end
            
            % Adding to meters
            if isempty(obj.meters.(meter_type))
                obj.meters.(meter_type) = PreHEAT_API.BuildingUnit.empty;
            end
            obj.meters.(meter_type)(end+1) = meter_2_add;               
        end
        
        function addZone(obj,zone_2_add)
            
            if isempty(zone_2_add)
                return;
            end
            
            % Adding the zone to the current object
            if isempty(obj.coveredZones)
                obj.coveredZones = PreHEAT_API.Zone.empty;
            end
            obj.coveredZones(end+1) = zone_2_add;
            
            % Linking the current unit to the zone
            if isempty(obj.parentUnit)
                obj.coveredZones(end).addLinkedUnit( obj );
            else
                obj.coveredZones(end).addLinkedUnit( obj, obj.parentUnit.type );
            end
        end
                
        function orderElementsById(obj)
            
            % Sorting subUnits, zones and controlUnits
            sub_element_names = {'subUnits','coveredZones','controlUnits'};
            
            for i=1:length(sub_element_names)
                element_i = sub_element_names{i};
                
                % Sorting units by id
                if ~isempty(obj.(element_i))
                    elements_id = [obj.(element_i).id];
                    [~,k_sorted_elements]  = sort( elements_id );
                    obj.(element_i)  = obj.(element_i)(k_sorted_elements);
                end
            end
            
            % For sub-meters, handling by types
            if ~isempty(obj.('meters'))
                
                sub_element_names = fieldnames(obj.('meters'));
                
                for i=1:length(sub_element_names)
                    submeter_i = sub_element_names{i};
                    
                    % Sorting units by id
                    if ~isempty(obj.('meters').(submeter_i))
                        elements_id = [obj.('meters').(submeter_i).id];
                        [~,k_sorted_elements]  = sort( elements_id );
                        obj.('meters').(submeter_i)  = obj.('meters').(submeter_i)(k_sorted_elements);
                    end
                end
            end
                        
            % Sorting coils by unit id
            if ~isempty(obj.coils)
                [~,k_sorted_elements]  = sort( [obj.coils.id] );
                obj.coils = obj.coils(k_sorted_elements);
            end
        end
        
        function [focus_component_details] = selectFocusComponents( obj, focus_components, add_prefix, add_prefix_subunits )
                        
            component_details = obj.getAllComponentDetails(add_prefix,add_prefix_subunits);
            
            % If not having a specific list of focus components
            if isempty(focus_components) || ( ischar(focus_components) && ...
                    ( strcmp(focus_components,'*') || strcmp(focus_components,'all') ) )
                focus_component_details = component_details;
                return;
            end
            
            % If focused on specific elements
            focus_component_details = PreHEAT_API.Unit.isolateFocusComponents(...
                component_details, focus_components);
        end
    
        function [options2use_limited] = limitComponentsSubunit(obj, options2use, sub_unit) 
            
            options2use_limited = options2use;
            components_to_select_from = options2use.limitComponents;
            
            if ischar(components_to_select_from) && ...
                    ( strcmp(components_to_select_from,'*') || strcmp(components_to_select_from,'all') )
                
                options2use_limited.limitComponents = components_to_select_from;
                
            elseif iscell(components_to_select_from)
                type_2_select = sub_unit.type;
                prefix2search = strcat(obj.type,'.',type_2_select);
                % Refining could be done on this criterion in the future.
                options2use_limited.limitComponents = ...
                    components_to_select_from(startsWith(components_to_select_from, prefix2search));
               
                options2use_limited.limitComponents = strrep( ...
                    options2use_limited.limitComponents, strcat(obj.type,'.'), '' );
                
            else
                error("Argument components_to_select_from must be a char ('*' or 'all') or cell.");                
            end
        end    
                  
        function [copy_obj] = copyElement(obj)
            copy_obj = copyElement@PreHEAT_API.Unit(obj);
            
            % Making deep copies of sub-units and control units
            for i=1:length(obj.subUnits)
                copy_of_obj.subUnits(i) = copy(obj.subUnits(i));
            end
            for i=1:length(obj.controlUnits)
                copy_of_obj.controlUnits(i) = copy(obj.controlUnits(i));
            end
            
            warning(...
                ['The copy functionality only makes a shallow copy of the following parameters: ',...
                'zones, controlUnits, meters, parentUnit, parentBuilding.']);
        end
                
        function [unitDetails] = getUnitDetails(obj, connection)
            unitDetails = table(obj.id,{obj.name},{obj.type},{connection},'VariableNames',{'id','name','type','connection'});
        end
    end
        
    methods (Static, Access='protected')
        
        function [options2use] = autocompleteOptionsLoading(options2use)
            
            options2use = autocompleteOptionsLoading@PreHEAT_API.Unit(options2use);
            
            if ~isfield(options2use, 'loadSubcomponentsInUnit') 
                options2use.loadSubcomponentsInUnit = 1;
            end
            if ~isfield(options2use, 'allowSubUnitsLoading') 
                options2use.allowSubUnitsLoading = 1;
            end
        end 
        
        function [system_names] = systemTypeNames()
            system_names = {'heating','hotWater','cooling','electricity','coldWater'};
        end
    end
end

