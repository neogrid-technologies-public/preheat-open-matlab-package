% Runs a full test of the API code
function [total_fails] = run_test()

% Setting up the tests
today_date  = dateshift( datetime('now','TimeZone','Europe/Copenhagen'), 'start', 'day' );

% Select the ID of the first test location
test_location_id = 2756;

PreHEAT_API.ApiConfig.setApiKey('KVkIdWLKac5XFLCs2loKb7GUitkTL4uJXoSyUFIZkVgWuCk8Uj');


fails_count = struct();

disp('---------------------------------------------------------');
disp('------  Test of the package PreHEAT_API.          -------');
disp('---------------------------------------------------------');

%% First, check that the different classes are installed and the API key is setup

fails_count.install = 0;

try
    fprintf('Test of the package PreHEAT_API installation [start]\n')
    
    % Checking the existence of the classes on the path
    classes_2_check = {...
        'PreHEAT_API.ApiConfig',    ...
        'PreHEAT_API.Building',     ...
        'PreHEAT_API.BuildingUnit', ...
        'PreHEAT_API.Component',  	...
        'PreHEAT_API.ControlUnit',  ...
        'PreHEAT_API.Data',         ...
        'PreHEAT_API.Pool',         ...
        'PreHEAT_API.Unit',         ...
        'PreHEAT_API.Weather',      ...
        'PreHEAT_API.Zone'          ...
        };
    
    for i=1:length(classes_2_check)
        fprintf(' * %s',classes_2_check{i});
        if exist(classes_2_check{i},'class')==8
            fprintf('[OK]\n')
        else
            subfunction_error( 'Class "%s" not found on path', classes_2_check{i} );
            fails_count.install = fails_count.install +1;
        end
    end
    
    % Creating the API key if it doesn't exist already
    fprintf(' * API key');
    if isempty(getenv('NEOGRID:PREHEAT_API:API_KEY'))
        PreHEAT_API.ApiConfig.setApiKey();
        % error('The API key is not installed. This can be done using the command: \ "PreHEAT_API.ApiConfig.setApiKey(XXX)" (where XXX is your key).')
        fprintf('[manually added]\n')
    else
        fprintf('[OK]\n')
    end
    
    if fails_count.install==0
        fprintf('[completed]\n\n');
    else
     	subfunction_error( '[completed with errors] (%i errors)\n\n', fails_count.install );        
    end
    
catch e
    subfunction_error(e);
    fails_count.install = fails_count.install +1;
end

%% Test of the Pool class
import PreHEAT_API.Pool;

fails_count.Pool = 0;

try
    fprintf('Test of the class PreHEAT_API.Pool [start]\n')
    try
        fprintf(' * Pool() ');
        test_pool = Pool();
        fprintf('[OK]\n');
    catch e        
        if strcmp(e.identifier,'PreHEAT:API:invalidKey')
            subfunction_error('Invalid API key')
            fails_count.Pool = fails_count.Pool + 1;
        else
            subfunction_error(e);
            fails_count.Pool = fails_count.Pool + 1;
        end
    end
    
    fprintf(' * Object public attributes ');
    if any(ismember({'numberOfBuildings','locationIds','addresses','zipcodes','cities','countries'},...
            fieldnames(test_pool))==0)
        subfunction_error('Fields are missing in the Pool object');
        fails_count.Pool = fails_count.Pool + 1;
        
    else
        % Check that the API key has access to at least one building
        if test_pool.numberOfBuildings==0
            subfunction_error('Your API key does not have access to any locations');
            fails_count.Pool = fails_count.Pool + 1;
        else            
            fprintf('[OK]\n');
        end
    end
    
    
    fprintf(' * Pool.getDetails() ');
    % Check that the details are available and not empty
    try
        pool_details = test_pool.getDetails();
        
        if isempty(pool_details)
            subfunction_error('The pool details were empty');
            fails_count.Pool = fails_count.Pool + 1;
        else
            fprintf('[OK]\n');
        end
        
    catch e
        subfunction_error(e);
        fails_count.Pool = fails_count.Pool + 1;
    end
    
    
    % Select a location id for the tests at building level    
    fprintf(' * Pool.getDetails() ');
    try
        test_pool.getBuilding(test_pool.locationIds(1));
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Pool = fails_count.Pool + 1;
    end
     
    fprintf(' * Pool.getUnitsOverview() ');
    try
        n2test   = min(5,test_pool.numberOfBuildings);
        ids2test = test_pool.locationIds([1:n2test]');
        
        all_units_table = Pool.getUnitsOverview(ids2test);
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Pool = fails_count.Pool + 1;
    end
    
    if fails_count.Pool==0
        fprintf('[completed]\n\n');
    else
        fprintf('[completed with errors] (%i errors) \n\n',fails_count.Pool);        
    end
    
catch e
    subfunction_error(e);
    fails_count.Pool = fails_count.Pool + 1;
end

%% Test of the Building class
import PreHEAT_API.Building;

fails_count.Building = 0;

try
    fprintf('Test of the class PreHEAT_API.Building [start]\n');
    
    fprintf(' * Building()');
    try
        test_building = Building( test_location_id );
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building public attributes ');
    % Testing the presence of the attributes of the Building class
    if any(ismember({'location','zones','units','weather','area','apartments','type'},fieldnames(test_building))==0)
        subfunction_error('Fields are missing in the Building object');
        fails_count.Building = fails_count.Building + 1;
    else
        fprintf('[OK]\n');
    end
    
    fprintf(' * Building.location fields ');
    % Testing that the location contains all the important elements
    if any(ismember({'locationId','address','zipcode','city','country','longitude','latitude','timezone'},...
            fieldnames(test_building.location))==0)
        subfunction_error('Fields are missing in the "location" parameter of the Building object');
        fails_count.Building = fails_count.Building + 1;
    else
        fprintf('[OK]\n');
    end
    
    fprintf(' * Building.units fields ');
    % Testing that the location contains all the important elements
    if any(ismember(...
            {'electricity','main','heating','hotWater','coldWater','indoorClimate','heatPumps','ventilation','custom'},...
            fieldnames(test_building.units))==0)
        subfunction_error('Fields are missing in the "units" parameter of the Building object');
        fails_count.Building = fails_count.Building + 1;
    else
        fprintf('[OK]\n');
    end
    
    fprintf(' * Building.(Common accessors) ');
    % Testing the methods describing characteristics
    try
        [z_id] = test_building.getLocationId();
        [z_address, z_zipcode, z_country] = test_building.getAddress();
        [z_id,z_address,z_organization,z_organization_id] = test_building.getLocationDetails();
        [z_org_name,z_org_id,z_par_org_name,z_par_org_id] = test_building.getOrganization();
        [z_latitude,z_longitude] = test_building.getCoordinates();
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.isempty ');
    % Testing the methods describing characteristics
    try
        if isempty(test_building)==0 && isempty(Building())==1
            fprintf('[OK]\n');
        else
            subfunction_error('wrong result of the function');
            fails_count.Building = fails_count.Building + 1;
        end
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.hasA()');
    hasA_possibilities =  {'main','electricity','coldWater','heating','hotWater',...
        'indoorClimate','heatPumps','ventilation','custom',...
        'hotWaterTank','hotWaterController','spaceHeatingController'};
    try
        for i=1:length(hasA_possibilities)
            test_building.hasA(hasA_possibilities{i});
        end
        fprintf('[OK]\n');
    catch e
        subfunction_error('argument: %s : [%s] %s',...
            hasA_possibilities{i},e.identifier,e.message );
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.getAllWeatherTypeIds()');
    try
        if ~isempty(test_building.getAllWeatherTypeIds) && ~istable(test_building.getAllWeatherTypeIds)
            subfunction_error('Building:getAllWeatherTypeIds did not return a table');
            fails_count.Building = fails_count.Building + 1;
        else
            fprintf('[OK]\n');
        end
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.getAllComponentIds()');
    try
        if ~isempty(test_building.getAllComponentIds) && ~istable(test_building.getAllComponentIds)
            subfunction_error('Building:getAllComponentIds did not return a table');
            fails_count.Building = fails_count.Building + 1;
        else
            fprintf('[OK]\n');
        end
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.getZonesDetails()');
    try
        test_building.getZonesDetails();
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.clearData()');
    try
        test_building.clearData();
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    % Test loadData
    fprintf(' * Building.loadData()');
    try
        resolution2test = {'raw','5min','hour','day','week','month','year'};
        for i=1:length(resolution2test)
            switch resolution2test{i}
                case 'raw'
                    dt_length2query = 3*hours(1);
                case '5min'
                    dt_length2query = 3*hours(1);
                case 'hour'
                    dt_length2query = 3*hours(1);
                case 'day'
                    dt_length2query = 3*caldays(1);
                case 'week'
                    dt_length2query = 3*calweeks(1);
                case 'month'
                    dt_length2query = 3*calmonths(1);
                case 'year'
                    dt_length2query = 3*calyears(1);
                otherwise
                    error('Unknown time resolution (%s)',resolution2test{i});
            end
            
            test_building.clearData();
            test_building.loadData( today_date-dt_length2query, today_date, resolution2test{i},...
                struct('limitComponents',{{'indoorClimate.temperature','indoorClimate.humidity','ventilation.*'}}) );
        end
        
        fprintf('[OK]\n');        
    catch e
        subfunction_error('Error in Building:loadData (location: %s / resolution: %s) : [%s] %s',...
            test_building.location.address, resolution2test{i}, e.identifier,e.message );
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.getData()');
    try
        % Loading some hours of data
        test_building.clearData();
        test_building.loadData( today_date-hours(6), today_date, 'hour' );
        
        test_building_data = test_building.getData();
        
        % Display the weather data in timetable format
        if ~istimetable(test_building_data.weather.data)
            subfunction_error('The weather data was not available');
            fails_count.Building = fails_count.Building + 1;
        else
            fprintf('[OK]\n');
        end
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.copy()');
    try
        % Loading some hours of data
        test_building_copy = copy(test_building);
        
        % Display the weather data in timetable format
        if test_building_copy==test_building
            subfunction_error('The copy and original object were identical');
            fails_count.Building = fails_count.Building + 1;
        elseif ~isempty(test_building_copy.weather.data)
            subfunction_error('The data in the unit is not reset');
            fails_count.Building = fails_count.Building + 1;
        else
            fprintf('[OK]\n');
        end
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.mergeData()');
    try
        % Loading some hours of data
        test_building_copy1 = copy(test_building);
        test_building_copy2 = copy(test_building);
        test_building_copy3 = copy(test_building);
        
        t_loading_start = datetime('yesterday','TimeZone','Europe/Copenhagen');
        t_split         = t_loading_start+hours(3);
        t_loading_end   = t_loading_start+hours(6)-seconds(1);
        test_building_copy1.loadData(t_loading_start,t_split-seconds(1),'hour');
        test_building_copy2.loadData(t_split,        t_loading_end,     'hour');
        
        % Merge the data (with actual data available)
        test_building_copy1.mergeData(test_building_copy2);
        T_weather2check1 = test_building_copy1.weather.data;
        
        % Merge the data (with one of the tables empty)
        test_building_copy1.mergeData(test_building_copy3);  
        test_building_copy3.mergeData(test_building_copy1);
        T_weather2check3 = test_building_copy3.weather.data;
        
        % Display the weather data in timetable format
        if T_weather2check1.getStartTime()~=t_loading_start || T_weather2check3.getStartTime()~=t_loading_start
            subfunction_error('The start time of the loaded data is bad');
            fails_count.Building = fails_count.Building + 1;
        elseif T_weather2check1.getEndTime()~=t_loading_end || T_weather2check3.getEndTime()~=t_loading_end
            subfunction_error('The end time of the loaded data is bad');
            fails_count.Building = fails_count.Building + 1;
            
        elseif height(T_weather2check1.data)~=ceil( (t_loading_end-t_loading_start)/hours(1) ) || ...
                height(T_weather2check3.data)~=ceil( (t_loading_end-t_loading_start)/hours(1) )                
            subfunction_error('The merging of data went bad');
            fails_count.Building = fails_count.Building + 1;
        else
            fprintf('[OK]\n');
        end
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    fprintf(' * Building.removeData()');
    try
        % Loading some hours of data
        test_building_copy1 = copy(test_building);
        
        t_test_start = datetime('yesterday','TimeZone','Europe/Copenhagen');
        t_test_end   = t_loading_start+hours(6)-seconds(1);
        test_building_copy1.loadData(t_test_start-hours(2), t_test_end+hours(2), 'hour');
        
        % Merge the data (with actual data available)
        test_building_copy1.removeData('before',t_test_start);
        test_building_copy1.removeData('after', t_test_end);
        T_weather2check1 = test_building_copy1.weather.data;
        
        % Display the weather data in timetable format
        if T_weather2check1.getStartTime()~=t_test_start
            subfunction_error('The start time of the cropped data is bad');
            fails_count.Building = fails_count.Building + 1;
        elseif T_weather2check1.getEndTime()~=t_test_end
            subfunction_error('The end time of the cropped data is bad');
            fails_count.Building = fails_count.Building + 1;
            
        elseif height(T_weather2check1.data)~=ceil( (t_test_end-t_test_start)/hours(1) )
            subfunction_error('The cropping of data went bad');
            fails_count.Building = fails_count.Building + 1;
        else
            fprintf('[OK]\n');
        end
    catch e
        subfunction_error(e);
        fails_count.Building = fails_count.Building + 1;
    end
    
    % % Tests missing for now (missing a generic test location to implement them on)
    %
    % test_building.importData
    % test_building.getWaterMeter
    % test_building.getHotWaterUnit
    % test_building.getElectricityMeter
    % test_building.getSpaceHeatingUnit
    % test_building.getMainMeter
    % test_building.getZone
    % test_building.getComfortProfile
    % test PreHEAT_API.ControlUnit;
    
    if fails_count.Building==0
        fprintf('[completed]\n\n');
    else
        fprintf('[completed with errors] (%i errors) \n\n',fails_count.Building);         
    end
catch e
    subfunction_error('[failed] ([%s] %s)\n\n',e.identifier,e.message);
end

%% Test of the Unit/Weather/BuildingUnit classes
import PreHEAT_API.Unit;
import PreHEAT_API.Weather;
import PreHEAT_API.BuildingUnit;

fails_count.Units = 0;
try
    fprintf('Test of the class PreHEAT_API.Unit [start]\n');
        
    fprintf(' * Unit()');
    try
        test_weather = test_building.weather;
        test_building_unit = test_building.units.indoorClimate(1);
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
    
    fprintf(' * Weather.loadData() ');
    try
        today_date = dateshift(datetime('now','TimeZone','Europe/Copenhagen'),'start','hour');
        test_weather.clearData();        
        test_weather.loadData( today_date-hours(6), today_date, 'hour', struct('limitComponents','*') );
        test_weather.clearData();
        test_weather.loadData( today_date-hours(6), today_date, 'hour', struct('limitComponents',{{'weather.Temperature','weather.WindDirection'}}) );
        test_weather.clearData();
        test_weather.loadData( today_date-hours(6), today_date, 'hour', struct('limitComponents',{{'weather.Temperature','weather.WindDirection'}}, 'addPrefix', 1) );
        test_weather.clearData();
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
        
    fprintf(' * BuildingUnit.loadData() ');
    try
        today_date = dateshift(datetime('now','TimeZone','Europe/Copenhagen'),'start','hour');
        test_building_unit.clearData();        
        test_building_unit.loadData( today_date-hours(6), today_date, 'hour', struct('limitComponents','*') );
        test_building_unit.clearData();
        test_building_unit.loadData( today_date-hours(6), today_date, 'hour', struct('limitComponents',{{'indoorClimate.temperature','indoorClimate.humidity'}}) );
        test_building_unit.clearData();
        test_building_unit.loadData( today_date-hours(6), today_date, 'hour', struct('limitComponents',{{'indoorClimate.temperature','indoorClimate.humidity'}}, 'addPrefix', 1) );
        test_building_unit.clearData();
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
        
    fprintf(' * BuildingUnit.copy()');
    try
        % Loading some hours of data
        test_building_unit_copy = copy(test_building_unit);
        
        % Display the weather data in timetable format
        if test_building_unit_copy==test_building_unit
            subfunction_error('The copy and original object were identical');
            fails_count.Units = fails_count.Units + 1;
        elseif ~isempty(test_building_unit_copy.data)
            subfunction_error('The data in the unit was not reset');
            fails_count.Units = fails_count.Units + 1;
        else
            fprintf('[OK]\n');
        end
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
    
    fprintf(' * BuildingUnit.updateState()');
    try
        % Loading the state of the unit
        test_building_unit.updateState();
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
    fprintf(' * BuildingUnit.getState()');
    try
        % Display the state data in table format
        T_state_building_unit = test_building_unit.getState();
        if istable(T_state_building_unit)
            fprintf('[OK]\n');            
        else
            subfunction_error('Unit.getState : the data returned was not a table');            
        end
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
    fprintf(' * Weather.updateState()');
    try
        % Updating the state of the weather
        test_weather.updateState();
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
    fprintf(' * Weather.getState()');
    try
        % Display the weather state in a timetable
        T_state_weather = test_weather.getState();
        if istable(T_state_weather)
            fprintf('[OK]\n');
        else
            subfunction_error('Weather.getState : the data returned was not a table');            
        end
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
        
    if fails_count.Units==0
        fprintf('[completed]\n\n');
    else
        fprintf('[completed with errors] (%i errors) \n\n',fails_count.Units);         
    end
catch e
    subfunction_error('[failed] ([%s] %s) \n\n',e.identifier,e.message);
end


%% Test of the Component class
import PreHEAT_API.Component;

fails_count.Component = 0;

try
    fprintf('Test of the class PreHEAT_API.Component [start]\n');
    
    component_details_test = struct('id',32618,'cid',1062540);
    fprintf(' * Component()');
    try
        test_component = Component( component_details_test );
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Component = fails_count.Component + 1;
    end
    
    fprintf(' * Component.loadData() ');
    test_component_data = test_building.units.main(1).components.supplyT;
    try
        today_date = dateshift(datetime('now','TimeZone','Europe/Copenhagen'),'start','hour');
        test_component_data.loadData( today_date-calmonths(18), today_date, '5min' );
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Component = fails_count.Component + 1;
    end
    
    fprintf(' * Component.getCurrentValue() ');
    try
        latest_value_1  = test_component_data.getCurrentValue();
        latest_value_2  = test_component_data.getCurrentValue( minutes(3) );
        latest_value_3  = test_component_data.getCurrentValue( minutes(3), datetime('now','TimeZone','Europe/Copenhagen') );
        fprintf('[OK]\n');
    catch e
        subfunction_error(e);
        fails_count.Component = fails_count.Component + 1;
    end
    
    fprintf(' * Component.copy()');
    try
        % Loading some hours of data
        test_component_copy = copy(test_component_data);
        
        % Display the weather data in timetable format
        if test_component_copy==test_component_data
            subfunction_error('The copy and original object were identical');
            fails_count.Component = fails_count.Component + 1;
        elseif ~isempty(test_component_copy.data)
            subfunction_error('The data in the unit was not reset');
            fails_count.Component = fails_count.Component + 1;
        else
            fprintf('[OK]\n');
        end
    catch e
        subfunction_error(e);
        fails_count.Units = fails_count.Units + 1;
    end
    
    if fails_count.Component==0
        fprintf('[completed]\n\n');
    else
        fprintf('[completed with errors] (%i errors) \n\n',fails_count.Component);         
    end
catch e
    subfunction_error('[failed] ([%s] %s) \n\n',e.identifier,e.message);
end

%% Test of the Data class
import PreHEAT_API.Data;

fails_count.Data = 0;

try
    fprintf('Test of the class PreHEAT_API.Data [start]\n');
    
    fprintf(' * Data()');
    
    fprintf(' * Data.containsComponent()');
    try
        test_building.weather.loadData( today_date-hours(6), today_date, 'hour' );
        if ~test_building.weather.data.containsComponent('Temperature')
            throw(MException('data:containsComponent','not detecting the field'));
        end
        if test_building.weather.data.containsComponent('doesnt_exist')
            throw(MException('data:containsComponent','wrongly detecting the field'));            
        end
        fprintf('[OK]\n'); 
    catch e
        subfunction_error(e);
        fails_count.Data = fails_count.Data + 1;        
    end
    
    if fails_count.Data==0
        fprintf('[completed]\n');
    else
        fprintf('[completed with errors] (%i errors) \n\n',fails_count.Data);         
    end
            
catch e
    subfunction_error('[failed] ([%s] %s) \n\n',e.identifier,e.message);
end

%% Tests that still require implementation (once the dummy location is created is created)

% %% Test of the Component class
% import PreHEAT_API.Component;
% 
% try
%     fprintf('Test of the class PreHEAT_API.Component [start]');    
%     fprintf(' * Component()');
%     fprintf('[OK]\n');    
%     fprintf('[completed]\n');
% catch e
%     subfunction_error('[failed] ([%s] %s)\n',e.identifier,e.message);
% end
% 
% %% Test of the Unit class
% import PreHEAT_API.Unit;
% 
% %% Test of the Data class
% import PreHEAT_API.Data;
% 
% import PreHEAT_API.ComfortProfile;
% 
% import PreHEAT_API.BuildingUnit;
% import PreHEAT_API.ControlUnit;
% import PreHEAT_API.Weather;
% 
% import PreHEAT_API.Zone;

%% Concluding the run

total_fails = 0;
fields_test = fieldnames(fails_count);
for i=1:length(fields_test)
    total_fails = total_fails + fails_count.(fields_test{i});
end

if total_fails==0
    fprintf('\n\n[test completed without errors] \n\n');    
else
    fprintf('\n\n[test completed with %i errors] \n\n',total_fails);    
end

disp('---------------------------------------------------------');
disp('-------   Test of package PreHEAT_API completed  --------');
disp('---------------------------------------------------------');

end

function [] = subfunction_error(varargin)

if length(varargin)==1 && ...
        isobject(varargin{1}) && ...
        all(ismember({'identifier','message','cause','stack','Correction'},fieldnames(varargin{1})))
    
    e = varargin{1};
    
    fprintf( '[NOK] ([%s] %s) \n', e.identifier, e.message );
else    
    fprintf( '[NOK] (%s) \n',sprintf(varargin{:}) );
end

end
